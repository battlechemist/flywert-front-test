import React from 'react';
import {connect} from 'react-redux';
import Layout from "@components/layout";
import AuthPage from '@components/pages/authPage'

export default function WithAuthHoc(Component) {
  return connect(state => ({state}))(
    class withAuth extends React.Component {

      static async getInitialProps(args) {
        const pageProps = await Component.getInitialProps && await Component.getInitialProps(args);

        return {...pageProps};
      }

      renderProtectedPage() {
        const isAuth = this.props.state?.auth?.isAuth;

        if (process.browser ? isAuth : this.props.auth) {
          return <Component {...this.props} />
        } else {
          return (
              <Layout {...this.props.auth}>
                <AuthPage />
              </Layout>
          )
        }
      }

      render() {
        return this.renderProtectedPage()
      }
    }
  )
}