import Cookies from "js-cookie";
import AlgoliaPlaces from "algolia-places-react";
import * as algoliasearch from "algoliasearch";
import React from "react";

export const getCookieFromReq = (req, cookie) => {
  const cookieFromServe = req.headers.cookie.split(';')
      .find(c => c.trim().startsWith(`${cookie}=`))

  if (!cookieFromServe) {
    return undefined
  }

  return cookieFromServe.split('=')[1]
}

export const orderDateFormat = date => {
  let newDate = new Date(date);
  let d = newDate.getDate();
  let m = newDate.getMonth() + 1;
  let y = newDate.getFullYear();
  return y + '-' + (m <= 9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
}


export const getUserAddress = () => {

  const algoliaClient = algoliasearch.initPlaces(process.env.ALGOLIA_APP_ID, process.env.ALGOLIA_API)

  return algoliaClient.reverse({
    aroundLatLng: `${Cookies.get("lat")},${Cookies.get("lng")}`,
    hitsPerPage: 1,
  }).then(response => {

    if (response.hits && response.hits.length) {
      const suggestion = response.hits[0];
      const {country_code} = suggestion;

      if(country_code) {
        Cookies.set('locale', country_code)
      }

      if (suggestion && suggestion.locale_names) {
        if (suggestion?.locale_names?.default[0]) {
          localStorage.setItem("locale_name", suggestion.locale_names.default[0]);
        }
      }

      if (suggestion && suggestion.city) {
        if (suggestion?.city?.default[0]) {
          localStorage.setItem("city_name", suggestion.city.default[0]);
        }
      }

      return !!(suggestion && (suggestion.locale_names || suggestion.city));
    }
  }).catch(err => {
    return false
  });
}


export const algoliaPlacesInput = (hideModal) => {

  const setInputAddress = ({ query, rawAnswer, suggestion, suggestionIndex }) => {
    const { lat, lng } = suggestion.latlng;
    Cookies.set("lat", lat);
    Cookies.set("lng", lng);

    if(suggestion.value) {
      localStorage.setItem("algolia_city_name", suggestion.value);
    }

    if(hideModal) {
      setTimeout(() => {
        hideModal()
      }, 200)
    }


  }

  return (
      <AlgoliaPlaces
          placeholder="Введите адрес"
          options={{
            appId: process.env.ALGOLIA_APP_ID,
            apiKey: process.env.ALGOLIA_API,
            language: "ru",
            countries: ["ru", "de", "ua"],
            type: "address",
          }}
          onChange={setInputAddress}
          onError={({ message }) =>
              console.error(
                  "Fired when we could not make the request to Algolia Places servers for any reason but reaching your rate limit."
              )
          }
      />
  )
}

//Check for empty local storage variable
export const notEmptyLS = (key = "") => localStorage.getItem(key) !== null

//Get local storage variable
export const getLSValue = (key) => notEmptyLS(key) ? localStorage.getItem(key) : ''




//Get an address determined by automatic geolocation
export const getAddressText = () => {
  if(notEmptyLS('algolia_city_name'))  return getLSValue('algolia_city_name')

  if (notEmptyLS('city_name') && notEmptyLS('locale_name')) return `${getLSValue('locale_name')} ${getLSValue('city_name')}`
}