export default {
  // Common
  homeTitle:
    "FlyWert.com — Оформить рабочую визу в Европу. Flywert.com — оформление визы, языковые курсы, эмиграция",
  formForgotPassword: "Забыли пароль?",
  formOr: "Или",
  password: "Пароль",
  register: "Регистрация",
  firstName: "Имя",
  lastName: "Фамилия",
  email: "E-mail:",
  male: "Мужской",
  female: "Женский",
  balance: "Баланс",
  // Header Button
  headLoginBtn: "Вход",
  headLogoutBtn: "Выйти",
  // Footer Menu
  footerMenuTitle: "Информация",
  footerMenuAbout: "О нас",
  footerMenuContact: "Контакты",
  footerMenuPartner: "Сотрудничество",
  footerMenuFaq: "FAQ",
  // Footer Payment Icons
  footerPaymentTitle: "Способы оплаты:",
  // Footer Social Links
  footerSocialLinks: "Социальные сети:",
  // Login modal
  authModalUserName: "Номер телефона или E-mail",
  authModalBtn: "Войти",
  authModalTitle: "Вход",
  // Register Modal
  regModalSmsBtn: "По смс",
  regModalEmailBtn: "По E-mail",
  regModalSendBtn: "Отправить",
  regModalConfPassword: "Подтвердите пароль",
  clickRegisterBtn: "Регистрируясь",
  regModalPrivacyPolicy:
    "Вы соглашаетесь с процедурой хранения и обработки персональных данных",
  changePhone: "Изменить номер телефона",
  smsCode: "Код из СМС",
  regContinue: "Продолжить регистрацию",
  regEnd: "Завершить регистрацию",
  regSuccess:
    "Регистрация завершена. Просьба подтвердить ваш email-адрес (см.почтовый ящик)",
};
