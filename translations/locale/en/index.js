export default {
  // Common
  homeTitle:
    "FlyWert.com - Apply for a work visa to Europe. Flywert.com - visa processing, language courses, emigration",
  formForgotPassword: "Forgot your password?",
  formOr: "or",
  password: "Password",
  register: "Register",
  firstName: "First name",
  lastName: "Last name",
  email: "E-mail:",
  male: "Male",
  female: "Female",
  balance: "Balance",
  // Header Button
  headLoginBtn: "Login",
  headLogoutBtn: "Logout",
  // Footer Menu
  footerMenuTitle: "Information",
  footerMenuAbout: "About us",
  footerMenuContact: "Contacts",
  footerMenuPartner: "Cooperation",
  footerMenuFaq: "FAQ",
  // Footer Payment Icons
  footerPaymentTitle: "Ways of payment:",
  // Footer Social Links
  footerSocialLinks: "Social networks:",
  // Login modal
  authModalUserName: "Phone number or E-mail",
  authModalBtn: "Sign in",
  authModalTitle: "Login",
  // Register Modal
  regModalSmsBtn: "SMS",
  regModalEmailBtn: "E-Mail",
  regModalSendBtn: "Send",
  regModalConfPassword: "Confirm password",
  clickRegisterBtn: "By registering",
  regModalPrivacyPolicy:
    "You agree with the procedure for storing and processing personal data",
  changePhone: "Change phone number",
  smsCode: "SMS code",
  regContinue: "Continue registration",
  regEnd: "Complete registration",
  regSuccess:
    "Registration is complete. Please confirm your email address (see mailbox)",
};
