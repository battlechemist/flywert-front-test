export default {
  // Common
  homeTitle:
    "FlyWert.com – Beantragen Sie ein Arbeitsvisum für Europa. Flywert.com – Visaabwicklung, Sprachkurse, Auswanderung",
  formForgotPassword: "Passwort vergessen?",
  formOr: "oder",
  password: "Passwort",
  register: "Registrieren",
  firstName: "Ihr Name",
  lastName: "Ihr Nachname",
  email: "E-mail:",
  male: "Männlich",
  female: "Weiblich",
  balance: "Guthaben",
  // Header Button
  headLoginBtn: "Einloggen",
  headLogoutBtn: "Ausloggen",
  // Footer Menu
  footerMenuTitle: "Information",
  footerMenuAbout: "Über uns",
  footerMenuContact: "Kontakte",
  footerMenuPartner: "Zusammenarbeit",
  footerMenuFaq: "FAQ",
  // Footer Payment Icons
  footerPaymentTitle: "Zahlungsarten:",
  // Footer Social Links
  footerSocialLinks: "Soziale Medien:",
  // Login modal
  authModalUserName: "Telefonnummer oder E-Mail",
  authModalBtn: "Einloggen",
  authModalTitle: "Zugangsberechtigung",
  // Register Modal
  regModalSmsBtn: "Per SMS",
  regModalEmailBtn: "Per E-Mail",
  regModalSendBtn: "Nachricht senden",
  regModalConfPassword: "Passwort bestätigen",
  clickRegisterBtn: "Mit Ihrer Registrierung",
  regModalPrivacyPolicy:
    "Sie stimmen der Speicherung und Verarbeitung personenbezogener Daten zu",
  changePhone: "Telefonnummer ändern",
  smsCode: "SMS-Code",
  regContinue: "Registrierung fortsetzen",
  regEnd: "Komplette Registrierung",
  regSuccess:
    "Die Registrierung ist abgeschlossen. Bitte bestätigen Sie Ihre E-Mail-Adresse (siehe Postfach)",
};
