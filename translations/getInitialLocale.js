import { defaultLocale } from './config'
import { isLocale } from './utils'
import Cookies from 'js-cookie'

export function getInitialLocale() {
  const localSetting = Cookies.get('locale')
  if (localSetting && isLocale(localSetting)) {
    return localSetting
  }

  const [browserSetting] = navigator.language.split('-')
  if (isLocale(browserSetting)) {
    return browserSetting
  }

  return defaultLocale
}