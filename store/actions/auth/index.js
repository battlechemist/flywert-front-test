import {
  register, confirmPhone,
  confirmCode, continuePhoneReg, confirmEmailRegister
} from "@services/auth.service";
import {helpersSetOneField} from "@store/actions/helpers";
import {HIDE_MODAL} from '../../constants/modal'
import {SET_TOKEN, AUTH_SET_ONE_FIELD, CLEAR_REGISTER_DATA, CLEAR_MESSAGE} from "../../constants/auth";
import Auth from '@services/auth';

export const RegisterAction = (data) => async dispatch => {
  helpersSetOneField('loading', true)(dispatch)
  return await register(data).then(response => {
    if (response.success) {
      dispatch({
        type: AUTH_SET_ONE_FIELD,
        key: 'emailRegStatus',
        payload: true
      })
      authSetOneField('message', response.message)(dispatch)
      authSetOneField('requestStatus', true)(dispatch)
      localStorage.getItem('resendEmail') && localStorage.removeItem('resendEmail')
      localStorage.getItem('registerActiveForm') && localStorage.removeItem('registerActiveForm')
    }
  }).catch(error => {
    if(error?.response?.data?.message) authSetOneField('message', error.response.data.message)(dispatch)
    authSetOneField('requestStatus', false)(dispatch)
  }).finally(() => {
    helpersSetOneField('loading', false)(dispatch)
  })
};

export const ConfirmEmailAction = (data) => async dispatch => {
  return new Promise((resolve, reject) =>
      confirmEmailRegister(data).then(response => {
        authSetOneField('message', response.message)(dispatch)
        authSetOneField('requestStatus', true)(dispatch)
        resolve(response)
      }).catch(error => {
        if(error?.response?.data?.message) authSetOneField('message', error.response.data.message)(dispatch)
        authSetOneField('requestStatus', false)(dispatch)
      })
  )
}

export const ConfirmPhoneAction = (data) => async dispatch => {
  helpersSetOneField('loading', true)(dispatch)
  return new Promise((resolve, reject) =>
      confirmPhone(data).then(response => {
        if (response.success) {
          dispatch({
            type: AUTH_SET_ONE_FIELD,
            key: 'phoneRegActiveForm',
            payload: 'code'
          })
        }
      }).catch(error => {
        if(error?.response?.data?.message) authSetOneField('message', error.response.data.message)(dispatch)
        authSetOneField('requestStatus', false)(dispatch)
      }).finally(() => {
        helpersSetOneField('loading', false)(dispatch)
      })
  )
}

export const ConfirmCodeAction = (data) => async dispatch => {
  helpersSetOneField('loading', true)(dispatch)
  return new Promise((resolve, reject) =>
      confirmCode(data).then(response => {
        if (response.success) {
          dispatch({
            type: AUTH_SET_ONE_FIELD,
            key: 'confirmCodeStatus',
            payload: true
          })
          dispatch({
            type: AUTH_SET_ONE_FIELD,
            key: 'phoneRegActiveForm',
            payload: 'email'
          })
        }
      }).catch(error => {
        if(error?.response?.data?.message) authSetOneField('message', error.response.data.message)(dispatch)
        authSetOneField('requestStatus', false)(dispatch)
      }).finally(() => {
        helpersSetOneField('loading', false)(dispatch)
      })
  )
}

export const ContinuePhoneRegAction = (data) => async dispatch => {
  helpersSetOneField('loading', true)(dispatch)
  return new Promise((resolve, reject) =>
      continuePhoneReg(data).then(response => {
        dispatch({
          type: AUTH_SET_ONE_FIELD,
          key: 'phoneRegActiveForm',
          payload: 'success'
        })
        clearRegisterData()(dispatch)
        authSetOneField('message', response.message)(dispatch)
        authSetOneField('requestStatus', true)(dispatch)
      }).catch(error => {
        if(error?.response?.data?.message) ('message', error.response.data.message)(dispatch)
        authSetOneField('requestStatus', false)(dispatch)
      }).finally(() => {
        helpersSetOneField('loading', false)(dispatch)
      })
  )
}


export const LoginAction = (data) => async dispatch => {
  helpersSetOneField('loading', true)(dispatch)
    console.log('LoginAction data', data)
  return new Promise((resolve, reject) =>
      Auth.login(data).then(res => {
        dispatch({
          type: AUTH_SET_ONE_FIELD,
          key: 'isAuth',
          payload: true
        })
        dispatch({type: HIDE_MODAL})
        clearRegisterData()(dispatch)
        resolve(res)
      }).catch(error => {
        if(error?.response?.data?.error_description) {
          authSetOneField('message', error.response.data.error_description)(dispatch)
        } else {
          authSetOneField('message', error.response.data.message)(dispatch)
        }

        authSetOneField('requestStatus', false)(dispatch)
      }).finally(() => {
        helpersSetOneField('loading', false)(dispatch)
      })
  )
};

export const LoginSocialAction = (data) => async dispatch => {
    helpersSetOneField('loading', true)(dispatch)
    //console.log('LoginSocialAction data', data)
    return new Promise((resolve, reject) =>
        Auth.loginSocial(data).then(res => {
            dispatch({
                type: AUTH_SET_ONE_FIELD,
                key: 'isAuth',
                payload: true
            })
            dispatch({type: HIDE_MODAL})
            clearRegisterData()(dispatch)
            resolve(res)
        }).catch(error => {
            if(error?.response?.data?.error_description) {
                authSetOneField('message', error.response.data.error_description)(dispatch)
            } else {
                authSetOneField('message', error.response.data.message)(dispatch)
            }

            authSetOneField('requestStatus', false)(dispatch)
        }).finally(() => {
            helpersSetOneField('loading', false)(dispatch)
        })
    )
};

export const LogoutAction = () => async dispatch => {
  Auth.logout().then(res => {
    if (res.success) {
      dispatch({
        type: AUTH_SET_ONE_FIELD,
        key: 'isAuth',
        payload: false
      })
    }
    dispatch({type: HIDE_MODAL})
  }).catch(err => {
    console.log("error", err)
  })
};

export const TokenAction = () => async dispatch => {
  return new Promise((resolve, reject) => {
        const payload = document.cookie.split('=')
        dispatch({
          type: SET_TOKEN,
          payload
        })
        resolve(payload)
      }
  )
};

export const authSetOneField = (key, message) => {
  return dispatch => {
    dispatch({
      type: AUTH_SET_ONE_FIELD,
      key,
      payload: message
    })
  }
}

export const clearRegisterData = () => {
  return dispatch => {
    dispatch({
      type: CLEAR_REGISTER_DATA
    })
  }
}

export const clearMessage = () => {
  return dispatch => {
    dispatch({
      type: CLEAR_MESSAGE
    })
  }
}
