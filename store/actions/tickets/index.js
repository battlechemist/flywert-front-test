import * as allConst from "../../constants/tickets";
import Cookies from "js-cookie";
import axios from "axios";
import {getLocale} from '@services/utils'
import {SET_USE_TICKET, SET_USE_TICKET_DATA} from '@store/constants/tickets'

const instance = axios.create({
  baseURL: process.env.base_url,
  headers: {
    'authorization': `Bearer ${Cookies.getJSON('access_token')}`,
    'X-Localization': getLocale(),
  }
})

export const setSelectedTicket = index => {
  return dispatch => {
    dispatch({
      type: allConst.SET_SELECTED_TICKET,
      payload: index ?? ''
    })
  }
}

export const setTicketData = data => {
  return dispatch => {
    dispatch({
      type: allConst.SET_USE_TICKET_DATA,
      payload: data
    })
  }
}

export const setSelectedTicketData = (data) => {
  return dispatch => {
    dispatch({
      type: allConst.SET_SELECTED_TICKET_DATA,
      payload: data
    })
  }
}

const usetTicketData = {
  filters: {
    lat: "50.5954140",
    lng: "36.5872680",
    range: "5"
  }
}

/*const usetTicketData = {
  filters: {
    lat: "50.5790000",
    lng: "36.5775000",
    range: "5"
  }
}*/



export const useTicket = (id) => async dispatch => {
  return await instance.post(`/use-ticket/${id}`, usetTicketData, {headers : {'authorization': `Bearer ${Cookies.getJSON('access_token')}`} }).then(response => {
    if(response?.data?.success) {
      dispatch({
        type: SET_USE_TICKET_DATA,
        payload: response.data.data
      })
    }
    dispatch({
      type: SET_USE_TICKET,
      payload: true
    })
  }).catch(error => {
    dispatch({
      type: SET_USE_TICKET,
      payload: false
    })
    console.log("error", error)
  })
}

