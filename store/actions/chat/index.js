import {GET_CHAT_MESSAGE_ARRAY, ADD_CHAT_MESSAGE} from "@store/constants/chat";


export const getChatMessageArray = (arr) => dispatch =>{
    dispatch({
        type: GET_CHAT_MESSAGE_ARRAY,
        payload: arr,
    })
}

export const addChatMessage = (obj) => dispatch =>{
    dispatch({
        type: ADD_CHAT_MESSAGE,
        payload: obj,
    })
}

