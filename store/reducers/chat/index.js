import * as allConst from '../../constants/chat'

const initialState = {
  userList: [
    {id: 0, avatar: '/img/pages/chat/chatItem1.svg', firstName: 'Андрей', lastName: 'Софронов', date: '15:43'},
    {id: 1, avatar: '/img/pages/chat/chatItem2.svg', firstName: 'Имя', lastName: 'Длиннаяфамилия', date: '11:23'},
    {id: 2, avatar: '/img/pages/chat/chatItem3.svg', firstName: 'Андрей', lastName: 'Софронов', date: '09:00'},
    {id: 3, avatar: '/img/pages/chat/chatItem4.svg', firstName: 'Анжела', lastName: 'Дубовская', date: '09:00'},
    {id: 4, avatar: '/img/pages/chat/chatItem5.svg', firstName: 'Сергей', lastName: 'Иванов', date: 'Вчера'},
    {id: 5, avatar: '/img/pages/chat/chatItem6.svg', firstName: 'Андрей', lastName: 'Софронов', date: 'Вчера'},
    {id: 6, avatar: '/img/pages/chat/chatItem2.svg', firstName: 'Имя', lastName: 'Длиннаяфамилия', date: '11:23'},
    {id: 7, avatar: '/img/pages/chat/chatItem3.svg', firstName: 'Андрей', lastName: 'Софронов', date: '09:00'},
    {id: 8, avatar: '/img/pages/chat/chatItem4.svg', firstName: 'Анжела', lastName: 'Дубовская', date: '09:00'},
    {id: 9, avatar: '/img/pages/chat/chatItem5.svg', firstName: 'Сергей', lastName: 'Иванов', date: 'Вчера'},
    {id: 10, avatar: '/img/pages/chat/chatItem1.svg', firstName: 'Андрей', lastName: 'Софронов', date: '15:43'},
    {id: 11, avatar: '/img/pages/chat/chatItem2.svg', firstName: 'Имя', lastName: 'Длиннаяфамилия', date: '11:23'},
    {id: 12, avatar: '/img/pages/chat/chatItem3.svg', firstName: 'Андрей', lastName: 'Софронов', date: '09:00'},
    {id: 13, avatar: '/img/pages/chat/chatItem4.svg', firstName: 'Анжела', lastName: 'Дубовская', date: '09:00'},
    {id: 14, avatar: '/img/pages/chat/chatItem5.svg', firstName: 'Сергей', lastName: 'Иванов', date: 'Вчера'},
    {id: 15, avatar: '/img/pages/chat/chatItem6.svg', firstName: 'Андрей', lastName: 'Софронов', date: 'Вчера'},
  ],
  messageList: [
    {id: 0, avatar: '/img/pages/chat/chatItem1.svg', status: 'in', firstName: 'Андрей', lastName: 'Софронов', date: 'Сегодня 16:22', message: 'Мой личный лайфхак: играть несколько раз подряд. Заходить на следующий день и повторить пункт 1. Я так уже выиграл гору скидок: от скидок в магазин техники до скидок на авиаперелет. А во 2 день «игры» мне удалось заполучить взрослый самокат в подарок. Не электрический, как я понял, но вполне себе крутой, не для детей. Буду на работу на нем гонять.!'},
    {id: 1, avatar: '/img/pages/chat/chatItem5.svg', status: 'out', firstName: 'Сергей', lastName: 'Иванов', date: '12.08.20 в 11:09', message: 'Не очень понимаю, я правда могу выиграть айфон? Случайным образом? Попробую испытать «теорию вероятности» на деле.'},
    {id: 2, avatar: '/img/pages/chat/chatItem6.svg', status: 'in', firstName: 'Андрей', lastName: 'Дубов', date: '2.10.20 в 17:40', message: 'Разнообразный и богатый опыт говорит нам, что консультация с широким активом предопределяет высокую востребованность первоочередных требований.'},
    {id: 3, avatar: '/img/pages/chat/chatItem4.svg', status: 'out', firstName: 'Анжела', lastName: 'Дубовская', date: '27.11.220 в 22:06', message: 'Равным образом, курс на социально-ориентированный национальный проект требует определения и уточнения дальнейших направлений развития. В своём стремлении повысить качество жизни, они забывают, что базовый вектор развития не оставляет шанса для направлений прогрессивного развития!'},
    {id: 4, avatar: '/img/pages/chat/chatItem3.svg', status: 'out', firstName: 'Имя', lastName: 'Длиннаяфамилия', date: '18.12.220 в 14:00', message: 'Ясность нашей позиции очевидна: убеждённость некоторых оппонентов обеспечивает актуальность первоочередных требований.'},
    ],
  chatMessageList: []
}

const ChatReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_MESSAGE':
      return {
        ...state
      }
    case 'GET_CHAT_MESSAGE_ARRAY':
      return {
        ...state,
        chatMessageList: action.payload
      }
    case 'ADD_CHAT_MESSAGE':
      return {
        ...state,
        chatMessageList: state.chatMessageList.concat(action.payload)
      }
    default:
      return state
  }
}

export default ChatReducer;
