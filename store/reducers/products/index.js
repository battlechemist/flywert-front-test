import * as allConst from '../../constants/products'

const initialState = {
  selectedProduct: '',
  products: ''
}

const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case allConst.SET_SELECTED_PRODUCT:
      return {
        ...state,
        selectedProduct: action.payload
      }
    case allConst.SET_PRODUCTS:
      return {
        ...state,
        products: action.payload
      }
    default:
      return state
  }
}

export default productReducer;