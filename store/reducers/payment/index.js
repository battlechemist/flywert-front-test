import * as allConst from '../../constants/payment'

const initialState = {
  paymentTypes: '',
  paymentStatus: '',
  paymentRequest: {
    status: '',
    message: ''
  },
  loading: false
}

const paymentReducer = (state = initialState, action) => {
  switch (action.type) {
    case allConst.SET_PAYMENT_TYPES:
      return {
        ...state,
        paymentTypes: action.payload
      }
    case allConst.SET_PAYMENT_STATUS:
      return {
        ...state,
        paymentStatus: action.payload
      }
    case allConst.SET_PAYMENT_PROCESS_MESSAGE:
      return {
        ...state,
        paymentRequest: {...action.payload}
      }
    case allConst.SET_PAYMENT_LOADING_STATUS:
      return {
        ...state,
        loading: !state.loading
      }
    default:
      return state
  }
}

export default paymentReducer