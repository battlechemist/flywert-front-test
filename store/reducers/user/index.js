import * as allConst from '../../constants/user'

const initialState = {
  userData: {}
}

const UserReducer = (state = initialState, action) => {
  switch (action.type) {
    case allConst.SET_USER_DATA:
      return {
        ...state,
        userData: {...action.payload}
      }
    default:
      return state
  }
}

export default UserReducer;