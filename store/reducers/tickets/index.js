import * as allConst from '../../constants/tickets'
import {SET_USE_TICKET_DATA} from "../../constants/tickets";

const initialState = {
  selectedTicket: '',
  selectedTicketData: '',
  useTicketStatus: false,
  useTicketData: ''
}

const ticketsReducer = (state = initialState, action) => {
  switch (action.type) {
    case allConst.SET_SELECTED_TICKET:
      return {
        ...state,
        selectedTicket: action.payload
      }
    case allConst.SET_SELECTED_TICKET_DATA:
      return {
        ...state,
        selectedTicketData: action.payload
      }
    case allConst.SET_USE_TICKET:
      return {
        ...state,
        useTicketStatus: action.payload
      }
    case allConst.SET_USE_TICKET_DATA:
      return {
        ...state,
        useTicketData: action.payload
      }
    default:
      return state
  }
}

export default ticketsReducer;