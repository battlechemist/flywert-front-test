import * as allConst from '../../constants/helpers'

const initialState = {
  loading: false,
  errorMessage: '',
  success: '',
}

const HelpersReducer = (state = initialState, action) => {
  switch (action.type) {
    case allConst.HELPERS_SET_ONE_FIELD:
      return {
        ...state,
        [action.key]: action.payload
      }
    case allConst.CLEAR_ERROR_MESSAGE:
      return {
        ...state,
        errorMessage: ''
      }
    default:
      return state
  }
}

export default HelpersReducer;