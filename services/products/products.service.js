import { BaseService } from '../base.service'

export class ProductsService extends BaseService {
  static get entity () {
    return 'get-products'
  }
}