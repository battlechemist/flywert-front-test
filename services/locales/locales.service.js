import { BaseService } from '../base.service'

export class LocalesService extends BaseService {
  static get entity () {
    return 'locales'
  }
}