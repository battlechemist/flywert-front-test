import { BaseService } from '../base.service'

export class UserService extends BaseService {
  static get entity () {
    return 'user'
  }
}

export class UploadUserAvatar extends BaseService {
  static get entity () {
    return 'user/avatar'
  }
}