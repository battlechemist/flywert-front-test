import Cookies from 'js-cookie'

export const getLocale = () => {
  return Cookies.get('locale') || 'ru'
}

export const requestErrorMessage = (error, key) => {
  if(error?.response?.data?.errors && error?.response?.data?.errors[key]) {
    return error.response.data.errors[key][0]
  }
}


export const phoneIsValid = phone => {
  const regex = /[^0-9*\/+-.(),\s]/g

  return !regex.test(phone)
}