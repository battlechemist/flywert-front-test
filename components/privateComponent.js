import {connect} from "react-redux";
import {isExpired} from "../services/auth.service";

const PrivateComponent = (props) => {
  return (
      <>
        {isExpired(props.expires_in) && props.children}
      </>
  )
}

const mapStateToProps = (state) => {
  return {
    expires_in: state.auth.expires_in
  }
}
export default connect(mapStateToProps, {})(PrivateComponent);