import {connect} from 'react-redux';
import Products from "../../products";

const MobMenuModal = ({products}) => {
  return <Products fullScreen={true} products={products}/>
}

const mapStateToProps = state => ({
  products: state.product.products
})

export default connect(mapStateToProps)(MobMenuModal)

