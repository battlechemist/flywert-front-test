import CreditCardForm from "./creditCardForm";
import StripeForm from './stripeForm';
import PaymentBalance from "./balance";
import css from './paymentModal.module.scss'

const WaysPayment = props => {
  const {
    paymentTypes,
    selectedPaymentType,
    setSelectedPaymentType,
    setPaymentTypeId,
    paymentRequest,
    setPaymentRequestMessage
  } = props;
  const {main_wallet, bonus_wallet} = props.userData;

  const onValueChange = (e, id) => {
    if(paymentRequest.message) {
      setPaymentRequestMessage()
    }
    setSelectedPaymentType(e.target.value)
    setPaymentTypeId(id)
  }

  const renderSelectedForm = ({driver}) => {
    if(driver === selectedPaymentType) {
      switch (selectedPaymentType) {
        case 'main-wallet':
          return <PaymentBalance balance={main_wallet} currency="EUR"/>
        case 'bonus-wallet':
          return <PaymentBalance balance={bonus_wallet}/>
        default:
          return ''
      }
    }
  }


  return (
      <div className={css.payment__methods}>
        {
          paymentTypes && paymentTypes.map(item => (
              <div className={css.payment__item} key={item.id}>
                <div>
                  <input
                      id={item.driver}
                      className={css.radioCustom}
                      type="radio"
                      value={item.driver}
                      checked={selectedPaymentType === item.driver}
                      onChange={e => onValueChange(e, item.id)}
                  />
                    <label htmlFor={item.driver} className={css.radioCustomLabel}>
                      {item.name}
                    </label>
                </div>
                {renderSelectedForm(item)}
              </div>
          ))
        }
      </div>
  )
}

export default WaysPayment;
