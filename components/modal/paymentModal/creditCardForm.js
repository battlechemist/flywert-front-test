import React from "react";
import {Formik, Field, useFormik} from 'formik';
import cx from 'classnames/bind'
import css from "./paymentModal.module.scss";

const CreditCardForm = () => {

  const validate = values => {
    const errors = {};

    if (!values.cardNumber) {
      errors.cardNumber = 'Required';
    }

    if (!values.expiryDate) {
      errors.expiryDate = 'Required';
    }

    if (!values.cvc) {
      errors.cvc = 'Required';
    }

    if (!values.privacyPolicy) {
      errors.privacyPolicy = 'Required';
    }

    return errors;
  };

  const formik = useFormik({
    initialValues: {
      cardNumber: '',
      expiryDate: '',
      cvc: '',
      privacyPolicy: true
    },
    validate,
    onSubmit: values => {
      console.log("values", values)
    },
  });

  return (
      <form onSubmit={formik.handleSubmit}>
        <div className={css.creditCard}>
          <p className={css.creditCard__title}>Оплата кредитной картой MasterCard с помощью системы Stripe</p>
          <div>
            <div className={css.creditCard__card_number}>
              <label htmlFor="cardNumber">Номер карты*</label>
              <input
                  id="cardNumber"
                  name="cardNumber"
                  type="text"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.cardNumber}
                  className={formik.touched.cardNumber && formik.errors.cardNumber && 'input__error'}
                  placeholder="4274 2568 6598 2456"
                  autoComplete="off"
              />
            </div>
            <div className={cx(css.creditCard__date_cvc, "flex")}>
              <div className={css.creditCard__date}>
                <label htmlFor="cardNumber">Номер карты*</label>
                <input
                    id="expiryDate"
                    name="expiryDate"
                    type="text"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.expiryDate}
                    className={formik.touched.expiryDate && formik.errors.expiryDate && 'input__error'}
                    placeholder="мм/гг"
                    autoComplete="off"
                />
              </div>
              <div className={css.creditCard__cvc}>
                <label htmlFor="cvc">Номер карты*</label>
                <input
                    id="cvc"
                    name="cvc"
                    type="text"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.cvc}
                    className={formik.touched.cvc && formik.errors.cvc && 'input__error'}
                    placeholder="CVC"
                    autoComplete="off"
                />
              </div>
            </div>
            <div>
              <input
                  id="privacyPolicy"
                  className={css.checkboxCustom}
                  type="checkbox"
                  value={formik.values.privacyPolicy}
                  checked={formik.values.privacyPolicy}
                  onChange={formik.handleChange}
              />
              <label
                  htmlFor="privacyPolicy"
                  className={cx(css.checkboxCustomLabel, css.creditCard__checkbox, formik.touched.privacyPolicy && formik.errors.privacyPolicy && 'input__error')}
              >
                Сохранить информацию об оплате на мой счет для будущих покупок
              </label>
            </div>
          </div>
          <button type="submit">
            Оплатить
          </button>
        </div>
      </form>
  )
}

export default CreditCardForm