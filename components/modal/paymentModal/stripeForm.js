import React, {useState} from 'react';
import {loadStripe} from '@stripe/stripe-js';
import {
  CardNumberElement,
  CardCvcElement,
  CardExpiryElement,
  Elements,
  useElements,
  useStripe,
} from '@stripe/react-stripe-js';

import {logEvent, Result, ErrorResult} from './strypeUtil';

const ELEMENT_OPTIONS = {
  style: {
    base: {
      fontSize: '18px',
      color: '#424770',
      letterSpacing: '0.025em',
      '::placeholder': {
        color: '#aab7c4',
      },
    },
    invalid: {
      color: '#9e2146',
    },
  },
};

const CheckoutForm = () => {
  const elements = useElements();
  const stripe = useStripe();
  const [name, setName] = useState('');
  const [postal, setPostal] = useState('');
  const [errorMessage, setErrorMessage] = useState(null);
  const [paymentMethod, setPaymentMethod] = useState(null);

  const handleSubmit = async (event) => {
    event.preventDefault();

    if (!stripe || !elements) return;

    const cardElement = elements.getElement(CardNumberElement);

    const payload = await stripe.createPaymentMethod({
      type: 'card',
      card: cardElement,
      billing_details: {},
    });

    if (payload.error) {
      console.log('[error]', payload.error);
      setErrorMessage(payload.error.message);
      setPaymentMethod(null);
    } else {
      console.log('[PaymentMethod]', payload.paymentMethod);
      setPaymentMethod(payload.paymentMethod);
      setErrorMessage(null);
    }
  };

  return (
      <form onSubmit={handleSubmit}>
        <label htmlFor="cardNumber">Card Number</label>
        <CardNumberElement
            id="cardNumber"
            onBlur={logEvent('blur')}
            onChange={logEvent('change')}
            onFocus={logEvent('focus')}
            onReady={logEvent('ready')}
            options={ELEMENT_OPTIONS}
        />
        <label htmlFor="expiry">Card Expiration</label>
        <CardExpiryElement
            id="expiry"
            onBlur={logEvent('blur')}
            onChange={logEvent('change')}
            onFocus={logEvent('focus')}
            onReady={logEvent('ready')}
            options={ELEMENT_OPTIONS}
        />
        <label htmlFor="cvc">CVC</label>
        <CardCvcElement
            id="cvc"
            onBlur={logEvent('blur')}
            onChange={logEvent('change')}
            onFocus={logEvent('focus')}
            onReady={logEvent('ready')}
            options={ELEMENT_OPTIONS}
        />
        {errorMessage && <ErrorResult>{errorMessage}</ErrorResult>}
        {paymentMethod && <Result>Got PaymentMethod: {paymentMethod.id}</Result>}
        <button type="submit" disabled={!stripe}>
          Pay
        </button>
      </form>
  );
};

const stripePromise = loadStripe('pk_test_51HuF3aIR6ssQCLHYqqV0331ghVcyQzSj1WR9S9S7QKO1HPln1MKsm1VRyS1p4tXc7puo7xgUIrMK8bdmYd9HS3dj00P4VgFWMC');

const App = () => {
  return (
      <Elements stripe={stripePromise}>
        <CheckoutForm />
      </Elements>
  );
};

export default App;