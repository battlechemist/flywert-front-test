import Image from "next/image";
import cx from 'classnames/bind';
import css from './AuthSocialLink.module.scss';
import rootModalCss from '../RootModal.module.scss';
import {useGoogleLogin} from "react-google-login";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import InstagramLogin from 'react-instagram-login';
import axios from 'axios';
import {token, tokenLocal} from "@services/token";
import Cookies from "js-cookie";
import {getLocale} from "@services/utils";
import {useState} from "react";
import {SET_USER_DATA} from "@store/constants/user";
import {connect, useDispatch} from "react-redux";
import {clearRegisterData, LoginSocialAction} from "@store/actions/auth";
import {getUserData} from "@store/actions/user";
import {AUTH_SET_ONE_FIELD} from "@store/constants/auth";
import {HIDE_MODAL} from "@store/constants/modal";
import { YandexLogin, YandexLogout } from 'react-yandex-login';


const clientID = '04d23c1f4f47467593e8c795b3637870';
const o_token = 'AgAAAAAOZDCvAAbXratVMpOafUginuIrvAn4i6E'

function ModalSocialIcons(props) {
    //console.log('props', props)

    const [userData, setUserData] = useState(undefined);

    const loginSuccess = async (userData) => {
        setUserData(userData)
        console.log('User Data: ', userData);
    }

    const handleYaAuth = async () => {
        try {
            const response = await fetch(
                "https://login.yandex.ru/info",
                {
                    //mode: 'no-cors',

                    method: "GET",
                    headers: {
                        Authorization: 'Bearer AgAAAAAOZDCvAAbY-w5xMlkKqEovubilZjNN7VU',
                        format: 'json',
                        Accept: '*/*',
                    },
                }
            );
            const item = await response.json();
            console.log(item);
        } catch (e) {
            console.error(e);
        }
    }

    const responseFacebook = async (response) => {
        //console.log('response', response);
        const requestGoogleUser = {
            "grant_type": "social",
            "client_id": process.env.client_id,
            "client_secret": "CyQaejvE9Tq2ykXW1aCz4aYpxU8OEpJngkVWjpHj",
            "access_token": response.accessToken,
            "provider": "facebook",
            //"first_name": response.profileObj.givenName,
            //"last_name": response.profileObj.familyName,
            "username": response.name,
            "email": response.email,
            "picture": response.picture.data.url
        }
        /*const instance = axios.create({
            baseURL: 'https://api.flywert-dev.ru/api/v1/',
            //baseURL: 'http://127.0.0.1:8000/api/v1/',
            headers: {'X-Localization': getLocale(),
                //'X-Localization': 'de',
                'Content-Type': 'application/json',
                'accept': 'application/json',
                //'authorization': `Bearer ${Cookies.getJSON('access_token')}`
                //Authorization: 'Bearer ' + tokenLocal,
            },
        });
        const oAuthData = await instance
            .post('oauth/token', requestGoogleUser)
            .then((res) => {
                console.log('res', res)
                if (res.data.success) {
                    return res.data.data;
                } else {
                    return [];
                }
            })
            .catch((error) => console.error(error));*/

        if (!props.loading) {
            props.LoginSocialAction(requestGoogleUser).then(res => {
                props.getUserData()
            })
        }
    }

    const loginSuccessGoogle = async (response) => {
        //console.log('response', response)
        const requestGoogleUser = {
            "grant_type": "social",
            "client_id": process.env.client_id,
            "client_secret": "CyQaejvE9Tq2ykXW1aCz4aYpxU8OEpJngkVWjpHj",
            "access_token": response.tokenObj.access_token,
            "provider": "google",
            "first_name": response.profileObj.givenName,
            "last_name": response.profileObj.familyName,
            "username": response.profileObj.email,
            "email": response.profileObj.email
        }
        /*const instance = axios.create({
            baseURL: 'https://api.flywert-dev.ru/api/v1/',
            //baseURL: 'http://127.0.0.1:8000/api/v1/',
            headers: {
                'X-Localization': getLocale(),
                //'X-Localization': 'de',
                'Content-Type': 'application/json',
                'accept': 'application/json',
                //'authorization': `Bearer ${Cookies.getJSON('access_token')}`
                //Authorization: 'Bearer ' + tokenLocal,
            },
        });
        const oAuthData = await instance
            .post('oauth/token', requestGoogleUser)
            .then((res) => {
                console.log('res', res)
                if (res.data.success) {
                    return res.data.data;
                } else {
                    return [];
                }
            })
            .catch((error) => console.error(error));*/

        if (!props.loading) {
            props.LoginSocialAction(requestGoogleUser).then(res => {
                props.getUserData()
            })
        }
    }
    const { signIn } = useGoogleLogin({
        onSuccess: loginSuccessGoogle,
        onFailure: loginSuccessGoogle,
        clientId: '114472176115-6fae9se7g334ucmhu8a55baii2jrtkcm.apps.googleusercontent.com',
        cookiePolicy: 'single_host_origin',
        isSignedIn: false,
    })

    const GoogleIcon = () => {
      return (
          <Image
              src='/img/icons/googleAuth.svg'
              alt='Google'
              width={18}
              height={18}
          />
      )
    }
    const VkIcon = () => {
        return (
            <Image
                src='/img/icons/vkAuth.svg'
                alt='Google'
                width={21}
                height={21}
            />
        )
    }
    const InstagramIcon = () => {
        return (
            <Image
                src='/img/icons/instagramAuth.svg'
                alt='Google'
                width={21}
                height={21}
            />
        )
    }
    const FacebookIcon = () => {
        return (
            <Image
                src='/img/icons/facebookAuth.svg'
                alt='Google'
                width={11}
                height={21}
            />
        )
    }
    const YandexIcon = () => {
        return (
            <Image
                src='/img/icons/yandexAuth.svg'
                alt='Google'
                width={20}
                height={20}
            />
        )
    }

    return (
      <div className={cx(rootModalCss.auth__social_links, 'flex flex__x_center')}>
        <ul className={cx(css.social_link_items, 'flex')}>
          {/*{
            socialLinkItems.map((item, index) => (
                <li key={index}>
                  <Image  {...item} key={index} />
                </li>
            ))
          }*/}
          <li>
              {!userData &&
              <YandexLogin clientID={clientID} onSuccess={loginSuccess}>
                  <button>Yandex Login</button>
              </YandexLogin>
              }
              {/*<button onClick={handleYaAuth}>
                  <YandexIcon/>
              </button>*/}
          </li>
            <li>
                <FacebookLogin
                    appId="876745139807990"
                    autoLoad={false}
                    fields="first_name, last_name ,email, picture"
                    scope="public_profile"
                    callback={responseFacebook}
                    disableMobileRedirect={true}
                    cssClass="fbtn"
                    render={renderProps => (
                        <button onClick={renderProps.onClick} type='submit' style={{borderRadius: '100%', width: '100%', height: '100%'}}>
                            <FacebookIcon/>
                        </button>
                    )}
                />
            </li>
            <li>
                <button onClick={ signIn } type='submit' style={{borderRadius: '100%', width: '100%', height: '100%'}}>
                    <GoogleIcon/>
                </button>
            </li>
        </ul>
      </div>
    )
}

const mapStateToProps = state => (
    {
        message: state.auth.message,
        loading: state.helpers.loading,
    }
)

export default connect(mapStateToProps, {getUserData, LoginSocialAction})(ModalSocialIcons)


