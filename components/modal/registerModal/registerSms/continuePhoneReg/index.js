import RegisterEmailForm from '../../registerEmail/registerEmailForm';
import RegisterSmsCss from '../RegisterSms.module.scss';

const ContinuePhoneReg = ({loading, confirmPhone, continuePhoneRegAction}) => {
  const formOnSubmit = (values) => {
    continuePhoneRegAction({...values, phone: confirmPhone})
  }

  return (
      <RegisterEmailForm
          btnTranslateKey={'regEnd'}
          handleSubmit={formOnSubmit}
          loading={loading}
          btnClassName={RegisterSmsCss.small_reg_btn}
      />
  )
}

export default ContinuePhoneReg;