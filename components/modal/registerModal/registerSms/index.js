import {connect} from 'react-redux';
import ConfirmPhone from './confirmPhone';
import ConfirmCode from './confirmCode';
import ContinuePhoneReg from './continuePhoneReg';
import {authSetOneField, ConfirmPhoneAction, ConfirmCodeAction, ContinuePhoneRegAction} from '@store/actions/auth'


const RegisterSms = (props) => {

  const compRender = (type) => {
    switch(type) {
      case 'code':
        return <ConfirmCode
            confirmPhone={props.confirmPhone}
            confirmCode={props.confirmCode}
            setOneField={props.authSetOneField}
            confirmCodeAction={props.ConfirmCodeAction}
        />
      case 'email':
        return <ContinuePhoneReg
            loading={props.loading}
            confirmPhone={props.confirmPhone}
            continuePhoneRegAction={props.ContinuePhoneRegAction}
        />
      default:
        return <ConfirmPhone
            confirmPhone={props.confirmPhone}
            setOneField={props.authSetOneField}
            confirmPhoneAction={props.ConfirmPhoneAction}
            loading={props.loading}
        />
    }
  }

  return (
      <div>
        {
          compRender(props.phoneRegActiveForm)
        }
      </div>
  )
}

const mapStateToProps = (state) => (
    {
      confirmPhone: state.auth.confirmPhone,
      confirmCode: state.auth.confirmCode,
      confirmPhoneStatus: state.auth.confirmPhoneStatus,
      phoneRegActiveForm: state.auth.phoneRegActiveForm,
      loading: state.helpers.loading
    }
)


export default connect(
    mapStateToProps,
    {authSetOneField, ConfirmPhoneAction, ConfirmCodeAction, ContinuePhoneRegAction}
)(RegisterSms)
