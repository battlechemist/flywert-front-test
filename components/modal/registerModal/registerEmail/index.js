import {connect} from "react-redux";
import {authSetOneField, RegisterAction} from "@store/actions/auth";
import RegisterEmailForm from "./registerEmailForm";


const RegisterEmail = (props) => {
  const formOnSubmit = (values) => {
    props.RegisterAction(values)
  }

  return (
      !props.emailRegStatus &&
      <RegisterEmailForm btnTranslateKey={'regModalSendBtn'} handleSubmit={formOnSubmit} loading={props.loading}/>
  )

}

const mapStateToProps = (state) => (
    {
      loading: state.helpers.loading,
      emailRegStatus: state.auth.emailRegStatus
    }
)


export default connect(
    mapStateToProps,
    {authSetOneField, RegisterAction}
)(RegisterEmail);