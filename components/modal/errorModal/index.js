import cx from 'classnames/bind'
import {connect} from 'react-redux'
import {hideModal} from '@store/actions/modal'
import css from './ErrorModal.module.scss'


const ErrorModal = (props) => {
  const {errorMessage, hideModal} = props;

  return (
      <div className={css.errorModal}>
        <div className={cx(css.errorModal__header, 'flex flex__x_end')}>
          <img src="/img/icons/errorModalClose.svg" alt="close" onClick={() => hideModal()} />
        </div>
        <div className={cx(css.errorModal__content, 'flex__center')}>
          <img src="/img/icons/warning.svg" alt="warning" />
          <p>{errorMessage}</p>
        </div>
      </div>
  )
}

export default connect(
    state => ({errorMessage: state.helpers.errorMessage}),
    {hideModal}
)(ErrorModal)