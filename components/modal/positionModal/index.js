import {algoliaPlacesInput} from '@helpers/utils'
import css from './PositionModal.module.scss'

const PositionModal = ({hideModal, showModal}) => {
  return (
      <div className={css.wrapper}>
        <div className={css.wrapper__title}>
          <p>Введите Ваш адрес</p>
        </div>
        <div className={css.wrapper__subtitle}>
          <p>Чтобы показать ближайшие к вам предложения, укажите Ваш город</p>
        </div>
        <div className={css.wrapper__search}>
          {algoliaPlacesInput(hideModal)}
        </div>
      </div>
  );
}

export default PositionModal