import {connect} from 'react-redux';
import {useFormik} from 'formik';
import cx from 'classnames/bind';
import Image from "next/image";
import {LoginAction} from '@store/actions/auth'
import rootModalCss from '../RootModal.module.scss';
import css from './LoginModal.module.scss';
import ModalSocialIcons from '../modalSocialIcons'
import useTranslation from '@hooks/useTranslation'
import {getUserData} from "@store/actions/user";
import {phoneIsValid} from "@services/utils";

const LoginModal = (props) => {
  const {t, locale} = useTranslation();
  //console.log('LoginModalProps', props)

  const setDataPhone = data => {
    if(phoneIsValid(data.username)) {
      return {
        phone: data.username,
        password: data.password
      }
    }
    return {...data}
  }

  const phoneRegex = /^\+\d+$/;
  const validate = values => {
    const errors = {};

    if (!values.username) {
      errors.username = 'Required';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.username) && !phoneRegex.test(values.username)) {
      if(!phoneRegex.test(values.username)) {
        errors.username = 'Email или номер телефона в международном формате';
      } else {
        errors.username = 'Invalid';
      }
    }

    if (!values.password) {
      errors.password = 'Required';
    } else if (values.password.length < 6) {
      errors.password = 'Must be 6 characters or less';
    }

    return errors;
  };

  const formik = useFormik({
    initialValues: {
      username: '',
      password: '',
    },
    validate,
    onSubmit: values => {
      if (!props.loading) {
        props.LoginAction(setDataPhone(values)).then(res => {
          props.getUserData()
        })
      }
    },
  });

  return (
      <div className={cx(rootModalCss.auth__modal, css.login__modal, 'flex__center')}>
        <span className={rootModalCss.auth__modal_close} onClick={() => props.hideModal()}>
          <Image src="/img/icons/closeModal.svg" alt="Close" width={15} height={15}/>
        </span>
        <div className={rootModalCss.modal__content}>
          <h3 className={cx(rootModalCss.modal__title, locale === 'de' && css.login__modal_title)}>{t('authModalTitle')}</h3>
          <div className={props.message && cx(rootModalCss.form__message_block, rootModalCss.error__message)}>
            {props.message}
          </div>
          <form onSubmit={formik.handleSubmit}>
            <input
                id="username"
                name="username"
                type="text"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.username}
                className={formik.touched.username && formik.errors.username && 'input__error'}
                placeholder="Номер телефона или E-mail"
            />
            <input
                id="password"
                name="password"
                type="password"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.password}
                className={formik.touched.password && formik.errors.password && 'input__error'}
                placeholder="Пароль"
            />
            <div className={rootModalCss.forgot__password}>
              <p>{t('formForgotPassword')}</p>
            </div>
            <button className={cx(rootModalCss.submit__btn, (props.loading || formik.errors.password || formik.errors.username) ? 'btn__disabled' : '')}
                    type="submit">{t('authModalBtn')}</button>
          </form>
          <div className={rootModalCss.or__auth}>
            <p>{t('formOr')}</p>
          </div>
          <ModalSocialIcons loading={props.loading} />
        </div>
      </div>
  )
}

const mapStateToProps = state => (
    {
      message: state.auth.message,
      loading: state.helpers.loading,
    }
)

export default connect(mapStateToProps, {LoginAction, getUserData})(LoginModal)
