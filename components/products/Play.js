function Play() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="30"
      height="30"
      fill="none"
      viewBox="0 0 30 30"
    >
      <circle cx="15" cy="15" r="14.5" stroke="#119AD0"></circle>
      <path fill="#119AD0" d="M12.313 9l8.5 6-8.5 6V9z"></path>
    </svg>
  );
}

export default Play;
