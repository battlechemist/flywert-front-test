import css from ".././Winners.module.scss";
import cx from "classnames/bind";
import Medal from ".././Medal";

export default function Winnders({ name, surname }) {
  return (
    <div className={css.winner}>
      <img
        className={cx(css.winner__avatar, "avatar")}
        src="/Andrey.jpg"
        alt="No Img"
      />
      <div className={css.winner__info}>
        <div className={css.winner__name}>{name}</div>
        <div className={css.winner__surname}>{surname}</div>
      </div>
      <div className={css.winner__medal} />
      {/* <Medal /> */}
    </div>
  );
}
