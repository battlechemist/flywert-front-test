import {useRef, useState} from 'react'
import cx from 'classnames/bind';
import {uploadUserAvatar, removeUserAvatar} from '@store/actions/user'
import css from './ProfileAvatar.module.scss'


const ProfileAvatar = props => {
  const {avatar, dispatch} = props;
  const fileInput = useRef(null);

  const fileInputHandleChange = e => {
    if (e?.target?.files[0]) {
      uploadUserAvatar(e.target.files[0])(dispatch)
    }
  }

  const deleteAvatarImage = () => {
    removeUserAvatar()(dispatch)
  }

  const renderAvatar = () => {
    if(avatar) {
      return (
          <img className={css.profileAvatar__avatar} src={avatar} alt="avatar"/>
      )
    } else {
      return (
          <div className={cx(css.profileAvatar__loadImage, "flex__center")}>
            <span>Загрузить фото</span>
          </div>
      )
    }
  }

  return (
      <div className={css.profileAvatar}>
        <label>
          <img className={css.profileAvatar__edit} src="/img/pages/profile/edit.png" alt="save"/>
          {renderAvatar()}
          <input type="file" ref={fileInput} onChange={fileInputHandleChange}/>
        </label>
        <div className={cx(css.profileAvatar__control, 'flex flex__between')}>
          <img  className={css.profileAvatar__remove} src="/img/pages/profile/trash.png" alt="remove" onClick={deleteAvatarImage} />
        </div>
      </div>
  )
}

export default ProfileAvatar