import {useEffect, useState} from 'react';
import cx from 'classnames/bind';
import Loader from "react-loader-spinner";
import {getUserOrders} from '@store/actions/user'
import {orderDateFormat} from '@helpers/utils'
import css from '../ProfileContent.module.scss';

const ProfileOrders = ({dispatch}) => {
  const [ordersData, setOrdersData] = useState('')
  const [message, setMessage] = useState('')
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true)

    getUserOrders()(dispatch).then(res => {
      if (res?.success) {
        if (res.data.length) {
          setOrdersData(res.data)
        } else {
          setMessage('Нет купленных билетов')
        }
      }
    }).finally(() => {
      setLoading(false)
    })
  }, [])

  const ordersRender = () => {
    if(ordersData.length) {
      return (
          <table>
            <tr className={css.orders__table_header}>
              <th>№</th>
              <th>Лотерея</th>
              <th>Дата</th>
              <th>Действия</th>
            </tr>
            {
              ordersData.map(item => (
                  <tr className={css.orders__item}>
                    <td>{item.order_item_id || ''}</td>
                    <td>
                      <div className={css.orders__item_product}>
                        <img src="/img/mock/prod4.svg" alt="product"/>
                        <div>
                          <p>{item.product_name || ''}</p>
                          <span>{item.product_price || ''} {item.product_price_currency}</span>
                        </div>
                      </div>
                    </td>
                    <td>{orderDateFormat(item.created_at)}</td>
                    <td>
                      {
                        item.use_time
                            ? <button className={cx(css.orders__btn, css.orders__btn_viewing)}>Просмотр</button>
                            : <button className={cx(css.orders__btn, css.orders__btn_check)}>Проверить</button>
                      }
                    </td>
                  </tr>
              ))
            }
          </table>
      )
    } else {
      return <p>{message}</p>
    }
  }

  return (
      <div className={css.orders}>
        <div className={css.profileInfo__title}>
          <p>Билеты</p>
        </div>
        <div className={css.orders__table_wrap}>
          {
            loading
            ? <div className={css.orders__loader_wrap}>
                  <Loader type="Oval" color="#4FB3EC" height={80} width={80} timeout={10000} visible={loading} />
                </div>
            : ordersRender()
          }
        </div>

      </div>
  )
}

export default ProfileOrders