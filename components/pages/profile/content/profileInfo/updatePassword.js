import {useFormik} from "formik";
import * as Yup from "yup";
import cx from 'classnames/bind';
import {updatePassword} from '@store/actions/user'
import rootModalCss from "../../../../modal/RootModal.module.scss";
import cssRegModal from '../../../../modal/registerModal/RegisterModal.module.scss';
import useTranslation from '../../../../../hooks/useTranslation';
import css from '../ProfileContent.module.scss'
import cssAuthPage from '../../../authPage/AuthPage.module.scss'


const UpdatePassword = (props) => {
  const {t, locale} = useTranslation();
  const {loading, dispatch, success} = props;

  const formik = useFormik({
    initialValues: {
      old_password:'',
      password: '',
      password_confirmation: '',
    },
    validationSchema: Yup.object({
      password: Yup.string().required('Password is required').min(6, 'Password is too short - should be 6 chars minimum.'),
      password_confirmation: Yup.string().required('Password is required').oneOf([Yup.ref('password'), null], 'Passwords must match'),
      old_password: Yup.string().required('Password is required').min(6, 'Password is too short - should be 6 chars minimum.'),
    }),
    onSubmit: values => {
      updatePassword(values)(dispatch)
    },
    enableReinitialize: true,
  });

  return (
      <div classsName={css.profileInfo}>
        <div className={cx(css.profileInfo__content, cssAuthPage.authPage)}>
            <form className={cx(css.updatePassword)} onSubmit={formik.handleSubmit}>
              <fieldset>
                <legend>Смена пароля</legend>
                <div className={cx(cssAuthPage.authPage__form_group, css.profileInfo__inputWrapp, css.profileInfo__inputInner)}>
                  <label htmlFor="first_name">Действующий пароль <span className="text-red">*</span></label>
                  <input
                      id="old_password"
                      name="old_password"
                      type="password"
                      placeholder={t('password')}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values.old_password}
                      className={formik.touched.old_password && formik.errors.old_password && 'input__error'}
                  />
                </div>
                <div className={cx(css.profileInfo__inputGroup, "flex flex__between")}>
                  <div className={cx(cssAuthPage.authPage__form_group, css.profileInfo__inputWrapp, css.profileInfo__inputInner)}>
                    <label htmlFor="password">Новый пароль <span className="text-red">*</span></label>
                    <input
                        id="password"
                        name="password"
                        type="password"
                        placeholder={t('password')}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.password}
                        className={formik.touched.password && formik.errors.password && 'input__error'}
                    />
                  </div>

                  <div className={cx(cssAuthPage.authPage__form_group, css.profileInfo__inputWrapp, css.profileInfo__inputInner)}>
                    <label htmlFor="password">Новый пароль <span className="text-red">*</span></label>
                    <input
                        id="password_confirmation"
                        name="password_confirmation"
                        type="password"
                        placeholder={t('regModalConfPassword')}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.password_confirmation}
                        className={formik.touched.password_confirmation && formik.errors.password_confirmation && 'input__error'}
                    />
                  </div>
                </div>
                <div className={cx(rootModalCss.btn__success, 'flex flex__y_center')}>
                  <button className={cx(rootModalCss.submit__btn, loading ? 'btn__disabled' : '')} type="submit">Сохранить</button>
                  {success === "updatePassword" ? <img src="/img/icons/check.png" alt="success"/> : ''}
                </div>
              </fieldset>
            </form>
        </div>
      </div>
  )
}

export default UpdatePassword
