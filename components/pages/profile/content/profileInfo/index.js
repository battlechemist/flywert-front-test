import {useFormik} from "formik";
import * as Yup from "yup";
import cx from 'classnames/bind';
import UpdatePassword from './updatePassword'
import {updateUserInfo} from '@store/actions/user'
import rootModalCss from "../../../../modal/RootModal.module.scss";
import cssRegModal from '../../../../modal/registerModal/RegisterModal.module.scss';
import useTranslation from '../../../../../hooks/useTranslation';
import css from '../ProfileContent.module.scss'
import cssAuthPage from '../../../authPage/AuthPage.module.scss'


const ProfileInfo = (props) => {
  const {t, locale} = useTranslation();
  const {loading, dispatch, success} = props;
  const {nickname, first_name, last_name, gender, email} = props.userData;

  const formik = useFormik({
    initialValues: {
      nickname: nickname || '',
      first_name: first_name || '',
      last_name: last_name || '',
      gender: gender || '',
      language: locale || 'ru',
      email: email || '',
    },
    validationSchema: Yup.object({
      nickname: Yup.string().required('Required').min(4, '4 chars minimum'),
      first_name: Yup.string().required('Required').min(4, '4 chars minimum'),
      last_name: Yup.string().required('Required').min(4, '4 chars minimum'),
      email: Yup.string().email('Invalid email address').required('Required')
    }),
    onSubmit: values => {
      const requestData = {
        values,
        email
      }
      updateUserInfo(requestData)(dispatch)
    },
    enableReinitialize: true,
  });

  return (
      <div className={cx(css.profileContainer)}>
        <div className={cx(css.profileInfo__content, cssAuthPage.authPage)}>
          <form className={cx(css.updatePassword)} onSubmit={formik.handleSubmit}>
            <fieldset>
              <legend>Мой аккаунт</legend>
              <div className={cx(css.profileInfo__inputGroup, "flex flex__between")}>
                <div className={cx(cssAuthPage.authPage__form_group, css.profileInfo__inputWrapp, css.profileInfo__inputInner)}>
                  <label htmlFor="first_name">Имя <span className="text-red">*</span></label>
                  <input
                      id="first_name"
                      name="first_name"
                      type="text"
                      autoComplete="off"
                      placeholder="Имя"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values.first_name}
                      className={formik.touched.first_name && formik.errors.first_name && 'input__error'}
                  />
                </div>

                <div className={cx(cssAuthPage.authPage__form_group, css.profileInfo__inputWrapp, css.profileInfo__inputInner)}>
                  <label htmlFor="password">Фамилия <span className="text-red">*</span></label>
                  <input
                      id="last_name"
                      name="last_name"
                      type="text"
                      placeholder="Фамилия"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values.last_name}
                      className={formik.touched.last_name && formik.errors.last_name  && 'input__error'}
                  />
                </div>
              </div>

              <div className={cx(css.profileInfo__inputGroup, "flex flex__between")}>
                <div className={cx(cssAuthPage.authPage__form_group, css.profileInfo__inputWrapp, css.profileInfo__inputInner)}>
                  <label htmlFor="password">Nickname <span className="text-red">*</span></label>
                  <input
                      id="nickname"
                      name="nickname"
                      type="text"
                      placeholder="Nickname"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values.nickname}
                      className={formik.touched.nickname && formik.errors.nickname  && 'input__error'}
                  />
                </div>
                <div className={cx(cssAuthPage.authPage__form_group, css.profileInfo__inputWrapp, css.profileInfo__inputInner)}>
                  <label htmlFor="password">Email <span className="text-red">*</span></label>
                  <input
                      id="email"
                      name="email"
                      type="email"
                      autoComplete="off"
                      placeholder={t('email')}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values.email}
                      className={formik.touched.email && formik.errors.email && 'input__error'}
                  />
                </div>
              </div>
              <div className={cx(cssAuthPage.authPage__form_group, css.profileInfo__inputWrapp)}>
                <div className={css.profileInfo__gender}>
                  <span>Пол</span>
                </div>
                <div className={cx(cssRegModal.gender__group, css.profileInfo__genderGroup, "flex")}>
                  <div className={cx(cssRegModal.gender__item, "flex flex__y_center")}>
                    <input
                        id="male"
                        type="radio"
                        value="male"
                        name='gender'
                        checked={formik.values.gender === 'male'}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                    />
                    <label htmlFor="male">{t('male')}</label>
                  </div>
                  <div className={cx(cssRegModal.gender__item, "flex flex__y_center")}>
                    <input
                        id="female"
                        type="radio"
                        value="female"
                        name='gender'
                        checked={formik.values.gender === 'female'}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                    />
                    <label htmlFor="female">{t('female')}</label>
                  </div>
                </div>
              </div>
              <div className={cx(rootModalCss.btn__success, 'flex flex__y_center')}>
                <button className={cx(rootModalCss.submit__btn, loading ? 'btn__disabled' : '')} type="submit">Сохранить</button>
                {success === 'profileInfo' ? <img src="/img/icons/check.png" alt="success"/> : ''}
              </div>
            </fieldset>
          </form>
        </div>
        <UpdatePassword loading={loading} dispatch={dispatch} success={success} />
      </div>
  )
}

export default ProfileInfo
