import css from '../ProfileContent.module.scss'

const ProfileConsole = props => {
  const {nickName, first_name, last_name} = props;

  return (
      <div className={css.profileInfo}>
        <div className={css.profileInfo__title}>
          <p>Мой аккаунт</p>
        </div>
        <div className={css.profileInfo__subTitle}>
          <p>Добро пожаловать, <span className={css.profileContent__activeText}>{nickName || `${first_name} ${last_name}`}</span></p>
        </div>
        <div className={css.profileInfo__desc}>
          <p>Из главной страницы аккаунта вы можете посмотреть ваши недавние заказы,
            настроить платежный адрес и адрес доставки, а также изменить пароль и основную информацию.</p>
        </div>
      </div>
  )
}

export default ProfileConsole;