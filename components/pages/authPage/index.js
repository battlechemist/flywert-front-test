import {connect} from 'react-redux';
import {useFormik} from 'formik';
import * as Yup from 'yup';
import cx from 'classnames/bind';
import {LoginAction} from '@store/actions/auth'
import useTranslation from '@hooks/useTranslation'
import rootModalCss from '../../modal/RootModal.module.scss';
import css from './AuthPage.module.scss'
import BreadCrumbs from "@components/breadCrumbs";

const authPage = (props) => {
  const {t, locale} = useTranslation();

  const formik = useFormik({
    initialValues: {
      username: 'some_user@flywert.com',
      password: 'some_user@flywert.com',
    },
    validationSchema: Yup.object({
      username: Yup.string().required('Required'),
      password: Yup.string().required('No password provided.').min(6, 'Password is too short - should be 8 chars minimum.')
    }),
    onSubmit: values => {
      if(!props.loading) {
        props.LoginAction(values)
      }
    },
  });

  return (
      <div className={css.authPage}>
        <BreadCrumbs breadCrumbActive="Авторизация"/>
        <div className={css.authPage__title}>
          <span>Авторизация</span>
        </div>
        <form onSubmit={formik.handleSubmit}>
          <div className={css.authPage__form_group}>
            <label htmlFor="username">Имя пользователя или email <span className="text-red">*</span></label>
            <input
                id="username"
                name="username"
                type="text"
                autoComplete="off"
                placeholder={t('authModalUserName')}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.username}
                className={formik.touched.username && formik.errors.username && 'input__error'}
            />
          </div>

          <div className={css.authPage__form_group}>
            <label htmlFor="password">Пароль <span className="text-red">*</span></label>
            <input
                id="password"
                name="password"
                type="password"
                placeholder={t('password')}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.password}
                className={formik.touched.password && formik.errors.password && 'input__error'}
            />
          </div>
          <button className={cx(rootModalCss.submit__btn, props.loading ? 'btn__disabled' : '')} type="submit">{t('authModalBtn')}</button>
        </form>
      </div>
  )
}

const mapStateToProps = state => (
    {
      message: state.auth.message,
      loading: state.helpers.loading,
    }
)

export default connect(mapStateToProps, {LoginAction})(authPage)