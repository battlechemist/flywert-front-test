import { useFormik } from "formik";
import * as Yup from "yup";
import cx from "classnames/bind";
import css from "@styles/pages/Partner.module.scss";

const PartnerForm = (props) => {
  const formik = useFormik({
    initialValues: {
      company_name: "",
      tax_number: "",
      email: "",
      phone: "",
      site_url: "",
      promo: "",
    },
    validationSchema: Yup.object({
      company_name: Yup.string().required("Required"),
      tax_number: Yup.string().required("Required"),
      email: Yup.string().email("Invalid email address").required("Required"),
      phone: Yup.number().required("Required"),
      // site_url: Yup.string().required("Required"),
      // promo: Yup.string().required("Required"),
    }),
    onSubmit: (values) => {
      console.log("PartnerForm", values);
    },
  });

  return (
    <div className={css.partner__form}>
      <h3>Общие сведения и контакты</h3>
      <form onSubmit={formik.handleSubmit}>
        <label
          htmlFor="company_name"
          className={cx(css.partner__requiredField, css.partner__label)}
        >
          Название вашей компании
        </label>
        <input
          id="company_name"
          name="company_name"
          type="text"
          autoComplete="off"
          placeholder="Название"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.company_name}
          className={
            formik.touched.company_name &&
            formik.errors.company_name &&
            "input__error"
          }
        />

        <label
          htmlFor="tax_number"
          className={cx(css.partner__requiredField, css.partner__label)}
        >
          Налоговый номер
        </label>
        <input
          id="tax_number"
          name="tax_number"
          type="text"
          autoComplete="off"
          placeholder="000000000000000"
          maxLength="15"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.tax_number}
          className={
            formik.touched.tax_number &&
            formik.errors.tax_number &&
            "input__error"
          }
        />

        <label
          htmlFor="email"
          className={cx(css.partner__requiredField, css.partner__label)}
        >
          Ваш E-mail
        </label>
        <input
          id="email"
          name="email"
          type="email"
          autoComplete="off"
          placeholder="email@mail.ru"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.email}
          className={
            formik.touched.email && formik.errors.email && "input__error"
          }
        />

        <label
          htmlFor="phone"
          className={cx(css.partner__requiredField, css.partner__label)}
        >
          Номер вашего телефона
        </label>
        <input
          id="phone"
          name="phone"
          type="tel"
          placeholder="+7 (___) ___ __ __"
          maxLength="15"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.phone}
          className={
            formik.touched.phone && formik.errors.phone && "input__error"
          }
        />

        <label htmlFor="site_url" className={cx(css.partner__label)}>
          Сайт
        </label>
        <input
          id="site_url"
          name="site_url"
          type="url"
          autoComplete="off"
          placeholder="Ссылка на сайт"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.site_url}
          className={
            formik.touched.site_url && formik.errors.site_url && "input__error"
          }
        />

        <label for="promo" className={cx(css.partner__label)}>
          Промокод
        </label>
        <input
          id="promo"
          name="promo"
          type="text"
          autoComplete="off"
          placeholder="Промокод"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.promo}
          className={
            formik.touched.promo && formik.errors.promo && "input__error"
          }
        />
      </form>
    </div>
  );
};

export default PartnerForm;
