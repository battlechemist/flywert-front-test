import cx from 'classnames/bind';
import css from './Chat.module.scss';

const UserList = ({ userList, heightContent }) => {
  return (
    <div
      className={cx(css.chat__userList, 'custom-scroll-y')}
      style={{ height: `${heightContent}px` }}
    >
      {userList.map((item) => (
        <div
          key={item.id}
          className={cx(css.chat__user, 'flex flex__y_center')}
        >
          <img src={item.avatar} alt={item.name} />
          <div>
            <p>{item.firstName}</p>
            <p>{item.lastName}</p>
          </div>
          <span>{item.date}</span>
        </div>
      ))}
    </div>
  );
};

export default UserList;
