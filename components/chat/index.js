import {useEffect, useState} from 'react'
import {connect} from 'react-redux'
import cx from 'classnames/bind'
import UserList from './userList'
import MessageList from "./messageList";
import css from './Chat.module.scss'
import axios from "axios";
import {getLocale} from "@services/utils";
import Cookies from "js-cookie";
import {SET_PRODUCTS} from "@store/constants/products";
import withLocale from "@hocs/withLocale";
import {tokenLocal, token} from "@services/token";
import Echo from "laravel-echo";
//import {addChatMessage} from "@store/actions/chat";
import {useDispatch} from "react-redux";
import {ADD_CHAT_MESSAGE} from "@store/constants/chat";


const Chat = ({ userList, messageList, messageData}) => {
    const [messageListHeight, setMessageListHeight] = useState(500)
    const [newChatMessage, setMessageArr] = useState({})
    const dispatch = useDispatch()

    const messageListHeightChange = height => {
    setMessageListHeight(height)
  }

    if (typeof window !== "undefined") {
        const chat = `chat.${getLocale()}`;

        window.Pusher = require('pusher-js');

        //window.Echo local
        /*window.Echo = new Echo({
            broadcaster: 'pusher',
            key: 1,
            // authEndpoint: '/custom/endpoint/auth',
            wsHost: window.location.hostname,
            wsPort: 6001,
            wssPort: 6001,
            forceTLS: false,
            disableStats: false,
            authEndpoint: "http://localhost:8000/broadcasting/auth",
            //encrypted: true,
            //enabledTransports: ['ws', 'wss'],
            auth:        {
              headers: {
                Authorization: 'Bearer ' + tokenLocal,
              },
            },
          });*/

        window.Echo = new Echo({
            broadcaster: 'pusher',
            key: 1,
            // authEndpoint: '/custom/endpoint/auth',
            wsHost: "api.flywert-dev.ru",
            wsPort: 6001,
            wssPort: 6001,
            forceTLS: true,
            disableStats: false,
            authEndpoint: "https://api.flywert-dev.ru/broadcasting/auth",
            //encrypted: true,
            enabledTransports: ['ws', 'wss'],
            auth:        {
                headers: {
                    Authorization: 'Bearer ' + token,
                    //authorization: `Bearer ${Cookies.getJSON("access_token")}`,
                },
            },
        });

        /*window.Echo.channel('chat.en').listen(".SharingChatNewMessage", (e) => {
            console.log('SharingChatNewMessage');
            console.log(e);
        });*/
        //showMessage(e.message.from.first_name,e.message.created_at,e.message.text);
        window.Echo.join('chat.en')
            .listen("ChatNewMessage", (e) => {
                console.log('new message got:', e);
                setMessageArr(e.message);
            })
            .here((users)=>{
                console.log('Here:');
                console.log(users);
            })
            .joining((user)=>{
                console.log(`Пришёл ${user.first_name} ${user.last_name}`);
            })
            .leaving((user)=>{
                console.log(user);
            });
    }

    if (Object.entries(newChatMessage).length !== 0) {
        //console.log('newChatMessage proceed')
        dispatch({
            type: ADD_CHAT_MESSAGE,
            payload: newChatMessage,
        });
    }

    return (
        <div className={css.chat}>
            <div className={css.chat__title}>
                <span>Чат</span>
                <img src="/img/pages/chat/chat.png" alt="chat"/>
            </div>
            <div className={cx(css.chat__inner, "flex")}>
                <UserList userList={userList} heightContent={messageListHeight}/>
                <MessageList messageData={messageData} messageListHeightChange={messageListHeightChange} />
            </div>
        </div>
    )
}

const mapStateToProps = state => (
    {
        userList: state.chat.userList,
        messageList: state.chat.messageList,
    }
)

export default connect(mapStateToProps, null)(Chat);
