import {useRef, useState, useEffect} from 'react';
import cx from 'classnames/bind'
import css from './Chat.module.scss'
import Echo from 'laravel-echo';
import {token, tokenLocal} from "@services/token";
import {getLocale} from "@services/utils";
import {connect} from "react-redux";


const MessageList = ({messageList, messageListHeightChange, messageData, chatMessageList, addMessage}) => {
  const chatTextarea = useRef(null);
  const messageContent = useRef(null);
  const messageListRef = useRef(null);
  const [textareaHeight, setTextareaHeight] = useState(20)
  const [messageListHeight, setMessageListHeight] = useState(389)

  const [messageInput, setMessageInput] = useState('')
  let [messagesArr, setData] = useState('')

  const urlPostMessages = 'https://api.flywert-dev.ru/api/v1/user/chat/message';
  const urlGetMessages = 'https://api.flywert-dev.ru/api/v1/user/chat/sharing';

  const messageForServer = {
    'text': messageInput.message,
    'private': false,
    /*'first_name': 'firstName',
    'last_name': 'lastName',
    'nickname': 'nickName',*/
  }

  function postMessageRequest(method, url, body = null) {
    try {
      const headers = {
        'Content-Type': 'application/json',
        'X-Localization': 'en',
        Authorization: 'Bearer ' + token,
        Accept: 'application/json',
      }
      return fetch(url, {
        method: method,
        body: JSON.stringify(body),
        headers: headers
      }).then(response => {
        if (response.ok) {
          return response.json()
        }
        return response.json().then(error => {
          console.log('Error:', error)
        })
      })
    }
    catch (e) {
      console.error(e)
    }
  }

  const handleSubmit = e => {
    e.preventDefault();
    if(chatTextarea?.current?.value?.length) {

      if(textareaHeight > 20) {
        messageListHeightChange(messageContent.current.offsetHeight - textareaHeight + 20)
        setTextareaHeight(20)
      }
      chatTextarea.current.value = ''
    }
    postMessageRequest('POST', urlPostMessages, messageForServer)
        .then(data => console.log(data))
        .catch(err => console.error(err))
  }

  const handleOnChange = e => {
    if(chatTextarea?.current?.scrollHeight && (textareaHeight < chatTextarea?.current?.scrollHeight)) {
      if(chatTextarea.current.offsetHeight < 100) {
        setTextareaHeight(chatTextarea?.current?.scrollHeight)

        if(messageListRef?.current?.offsetHeight) {
          setMessageListHeight(messageListRef.current.offsetHeight + 30)
        }
        if(messageContent?.current?.offsetHeight) {
          messageListHeightChange(messageContent.current.offsetHeight + 21)
        }
      }
    }
    setMessageInput(prev => ({...prev, ...{
        [e.target.name]: e.target.value
      }}))

  }

  const dumbAvatar = "/img/pages/chat/chatItem1.svg";

  return (
      <div className={css.chat__content} ref={messageContent}>
        <div className={cx(css.chat__messageList, 'custom-scroll-y')} ref={messageListRef} style={{height: messageListHeight}}>
          {
            chatMessageList.map(item => (
                <div key={item.id} className={css.chat__message}>
                  <div className={cx(css.chat__messageHeader, 'flex flex__y_center')}>
                    <img src={dumbAvatar} alt={item.nickname}/>
                    <span>{`${item.from.first_name} ${item.from.last_name}`}</span>
                  </div>
                  <div className={cx(css.chat__messageContent, item.status === 'in' ? css.chat__messageIn : css.chat__messageOut)}>
                    <p>{item.text}</p>
                  </div>
                  <div className={cx(css.chat__messageFooter, 'flex flex__x_end')}>
                    <p>{item.created_at}</p>
                  </div>
                </div>
            ))
          }
        </div>
        <div className={css.chat__form}>
          <form onSubmit={handleSubmit}>
            <div className={css.chat__form__inner}>
              <textarea
                  style={{height: `${textareaHeight}px`}}
                  rows="1"
                  placeholder="Ваше сообщение"
                  id="input-chat"
                  className="input-chat custom-scroll-y"
                  name="message"
                  onChange={handleOnChange}
                  ref={chatTextarea}
              />
              <button type="submit">
                <span>Отправить</span>
                <img src="/img/pages/chat/sendMessage.svg" alt="send"/>
              </button>
            </div>
          </form>
        </div>
      </div>
  )
}
const mapStateToProps = state => (
    {
      chatMessageList: state.chat.chatMessageList,

    }
)

export default connect(mapStateToProps, null)(MessageList);

