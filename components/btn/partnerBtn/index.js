import Link from "next/link";
import useTranslation from "../../../hooks/useTranslation";
import css from "./PartnerBtn.module.scss";

export default function PartnerBtn() {
  const { locale } = useTranslation();

  return (
    <Link href={`/${locale}/partner`}>
      <button className={css.partner__btn}>
        <span> Станьте нашим партнером </span>
      </button>
    </Link>
  );
}
