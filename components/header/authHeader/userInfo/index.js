import Link from 'next/link'
import cx from 'classnames/bind';
import useTranslation from '../../../../hooks/useTranslation';
import css from './UserInfo.module.scss';

const UserInfo = props => {
  const { t, locale } = useTranslation()
  const {avatar, first_name, nickname, main_wallet, bonus_wallet} = props.userData


  return (
      <div className={cx(css.user__info_vn, 'flex flex__x_end flex__y_center')}>
        <div className={css.userName}>
          <Link href={`/${locale}/profile`}>
            <a>
              <div className={cx('flex flex__y_center', css.header__user_info)}>
                {avatar ? <img className={css.header__avatar} src={avatar} alt="User avatar"/> : '' }
                <p className={css.user_info_desc}>{nickname || first_name}</p>
              </div>
            </a>
          </Link>
        </div>
        <div className={css.header__user_balance}>
          <span className={css.user_info_desc}>
            {t('balance')}:
            <span className={css.user__balance_value}> {`${main_wallet || 0} ${main_wallet > 0 ? 'EUR' : ''} / ${bonus_wallet || 0}`}</span>
          </span>
        </div>
      </div>
  )
}

export default UserInfo;