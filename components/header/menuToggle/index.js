import Image from 'next/image';
import cx from 'classnames/bind'
import css from '../Header.module.scss';

export default function MenuToggle({showModal, modalType, hideModal}) {
  const modalChange = () => {
    if(modalType === 'MOB_MENU_MODAL') {
      hideModal()
    } else {
      showModal('MOB_MENU_MODAL')
    }
  }

  return (
      <div className={css.menu__toggle} onClick={modalChange}>
        <span className={cx(css.menu__toggle_icon, modalType === 'MOB_MENU_MODAL' ? css.active : '')} />
      </div>
  )
}
