import Image from "next/image";
import Link from "next/link";
import css from "./FooterSocialIcons.module.scss";
import footerCss from "@components/footer/Footer.module.scss";
import useTranslation from "../../../hooks/useTranslation";

export default function FooterSocialLink() {
  const { t } = useTranslation();
  const socialLinkItems = [
    {
      alt: "Instagram",
      src: "/img/icons/insta.svg",
      link: "https://www.instagram.com/flywertcom/",
      width: 15,
      height: 15,
    },
    {
      alt: "Facebook",
      src: "/img/icons/fb.svg",
      link: "https://www.facebook.com/groups/972741186509037",
      width: 15,
      height: 15,
    },
    {
      alt: "Youtube",
      src: "/img/icons/youtube.svg",
      link: "https://www.youtube.com/channel/UCDBd2uBVp2obmv9K4ecOsXw",
      width: 17,
      height: 17,
    },
    {
      alt: "Vkontakte",
      src: "/img/icons/vk.svg",
      link: "https://vk.com/public197939506",
      width: 14,
      height: 8,
    },
  ];
  return (
    <div className={css.footer__social__link}>
      <p className={footerCss.footer__item_title}>{t("footerSocialLinks")}</p>
      <ul className={css.social__link_items}>
        {socialLinkItems.map((item, index) => (
          <li key={index}>
            <Link href={item.link}>
              <a target="_blank">
                <Image {...item} key={index} />
              </a>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
}
