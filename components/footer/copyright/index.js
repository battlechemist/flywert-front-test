import css from './Copyright.module.scss'

export default function Copyright() {
  return (
      <div className={css.copyright}>
        <p>© FlyWert 2020</p>
      </div>
  )
}