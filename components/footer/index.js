import cx from 'classnames/bind';
import css from './Footer.module.scss';
import Logo from '@components/logo';
import FooterMenu from './footerMenu';
import FooterPayment from './footerPayment';
import FooterSocialLink from './footerSocialIcons';
import Copyright from './copyright';

export default function Footer() {
  return (
    <footer>
      <div className={css.footer__container}>
        <div className="navContainer, container">
          <div className={cx(css.footer__group, 'flex flex__between')}>
            <Logo className={css.footer__logo} />
            <FooterMenu />
            <div className={cx(css.footer__inner_group, 'flex flex__between')}>
              <FooterPayment />
              <FooterSocialLink />
            </div>
          </div>
        </div>
      </div>
      <Copyright />
    </footer>
  );
}
