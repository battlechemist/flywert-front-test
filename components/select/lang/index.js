import styles from './LangSelect.module.scss';

export default function LangSelect() {
  return (
    <div className={styles.lang}>
      <label className="visuallyHidden" htmlFor="lang" >
        Выбрать язык
      </label>

      <select className={styles.lang__select} name="lang" id="lang">
        <option value="ru" className={styles.lang__option}>ru</option>
        <option value="en" className={styles.lang__option}>en</option>
      </select>
    </div>
  );
}

