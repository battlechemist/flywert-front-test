import Image from 'next/image'
import Link from 'next/link'

export default function Logo(props) {
  return (
      <div className={props.className || ''}>
        <Link href="/">
          <a><Image src="/img/icons/mainLogo.svg" alt="Логотип" width={136} height={136} /></a>
        </Link>
      </div>
  );
}
