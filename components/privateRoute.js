import React, {Component, useEffect} from "react";
import ServerCookie from "next-cookies";
import {getTokenFields, isExpired} from "../services/auth.service";


export function privateRoute(WrappedComponent) {
    return class extends Component {
        static async getInitialProps(ctx) {
            const tokenFields = ServerCookie(ctx);
            const initialProps = {tokenFields};
            if (!isExpired(tokenFields['expires_in'])) {
                ctx.res.writeHead(302, {
                    Location: "/",
                });
                ctx.res.end();
            }
            if (WrappedComponent.getInitialProps) return WrappedComponent.getInitialProps(initialProps);
            return initialProps;
        }

        render() {
            return <WrappedComponent {...this.props} />;
        }
    };
}