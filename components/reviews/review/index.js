import React, { useState } from "react";
import Star from "../StarSvg";
import Modal from "react-modal";
import ReactPlayer from "react-player";
import css from "../Reviews.module.scss";

export const ShowStars = ({ score }) => {
  let stars = [];
  for (let i = 0; i < score; i++) {
    stars.push(<Star key={i} />);
  }
  return stars;
};

export default function Review({ review }) {
  const { full_name, review_text, image_url, video, length } = review;

  const [isVisibility, setVisibility] = useState("false");

  const handleToggleVisibility = () => {
    setVisibility(!isVisibility);
  };

  const ShowMore = () => {
    if (review_text.split(" ").length > length) {
      return (
        <p className={css.review__showMore} onClick={handleToggleVisibility}>
          {isVisibility ? "Читать дальше" : "Свернуть текст"}
        </p>
      );
    }
    return "";
  };

  let customStyles = {
    overlay: {
      backgroundColor: "rgba(0, 0, 0, 0.85)",
    },
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginTop: "0",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
      border: "none",
      background: "none",
      padding: "24px 0",
      maxWidth: "970px",
    },
  };

  const [modalIsOpen, setIsOpen] = useState(false);

  function openModal() {
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
  }

  return (
    <div className={css.review}>
      <div className={css.review__header}>
        <div className={css.review__iconPlay} onClick={openModal}>
          <img src={image_url} className="avatar" />
        </div>
        <Modal
          className={css.review__videoModal}
          isOpen={modalIsOpen}
          onRequestClose={closeModal}
          style={customStyles}
        >
          <div className={css.review__videoWrapper}>
            <span
              className={css.review__closeModal}
              onClick={closeModal}
            ></span>
            <ReactPlayer
              className={css.review__videoPlayer}
              url={video}
              width="100%"
              height="100%"
              controls={true}
            />
          </div>
        </Modal>

        <div className={css.review__right}>
          <div className={css.review__name}>{full_name}</div>
          <div className={css.review__score}>
            <ShowStars score={5} />
          </div>
        </div>
      </div>
      <div className={css.review__body}>
        {isVisibility ? (
          <p>
            {review_text
              .split(" ")
              .slice(0, length - 1)
              .join(" ")}
            {length - 1 < review_text.split(" ").length ? "..." : ""}
          </p>
        ) : (
          <p>{review_text}</p>
        )}
        <ShowMore />
      </div>
    </div>
  );
}
