import React, { useState } from "react";
import Review from "./review";
import PrimaryBtn from "../../components/btn/primaryBtn/";
import css from "./Reviews.module.scss";
import cx from "classnames/bind";

/*const reviews = [
  {
    full_name: "Ольга (Павлодар)",
    review_text:
      "Всем привет! Хочу поделиться с вами радостной новостью.Слежу за forever in Germany давно. Увидела и наткнулась на их новый сайт visa-lucky.com. Здесь проводились и проводятся различные конкурсы. На протяжении долгого времени следила за этим сайтом.",
    image_url:
      "https://s.abcnews.com/images/GMA/191211_gma_thunberg1_hpMain_16x9_992.jpg",
    video: "https://www.youtube.com/watch?v=gRuZY1IXsMc",
    length: 34,
    created_at: "2020-11-06T13:42:17.000000Z",
    updated_at: "2020-11-06T13:42:17.000000Z",
  },
  {
    full_name: "Ольга (Павлодар)",
    review_text:
      "Всем привет! Хочу поделиться с вами радостной новостью.Слежу за forever in Germany давно. Увидела и наткнулась на их новый сайт visa-lucky.com. Здесь проводились и проводятся различные конкурсы. На протяжении долгого времени следила за этим сайтом.",
    length: 34,
    image_url:
      "https://s.abcnews.com/images/GMA/191211_gma_thunberg1_hpMain_16x9_992.jpg",
    video: "https://www.youtube.com/watch?v=gRuZY1IXsMc",
    created_at: "2020-11-06T13:42:17.000000Z",
    updated_at: "2020-11-06T13:42:17.000000Z",
  },
];*/

export default function Reviews({ reviews }) {
  reviews.map((item) => {
    item.length = 34;
  });

  function ShowReview() {
    return reviews.map((review, i) => <Review review={review} key={i} />);
  }

  const [isVisibility, setVisibility] = useState("false");

  const handleToggleVisibility = () => {
    setVisibility(!isVisibility);
  };

  return (
    <div className={css.reviewsContainer}>
      <h1 className={css.header}>Отзывы победителей</h1>
      <div className={isVisibility ? css.reviews : css.reviewsAll}>
        <ShowReview />
      </div>
      <PrimaryBtn
        name={isVisibility ? "Все отзывы" : "Свернуть"}
        className={cx(css.btn__reviews, "btn-primary")}
        onClick={handleToggleVisibility}
      />
    </div>
  );
}
