import { useEffect } from 'react';
import ReactPlayer from "react-player/youtube";
import cx from "classnames/bind";
import Layout from "@components/layout";
import css from "@styles/pages/Partner.module.scss";
import PartnerForm from "@components/pages/partner/partnerForm";
import PartnerFilial from "@components/pages/partner/partnerFilial";
import { useState } from "react";
import GeneralPartnerForm from "@components/pages/partner/generalPartnerForm";

const Partner = () => {
  const [chosenDiscount, setChosenDiscount] = useState("");
  const [urlVideo, setUrlVideo] = useState("");
  const [loading, setLoading] = useState(false);
  const [toDate, setToDate] = useState("");
  const [partnerContacts, setPartnerContacts] = useState([]);
  const [discountObject, setDiscountObject] = useState("");

  const showVideo = async () => {
    // async function showVideo() {
    setLoading(true);
    try {
      const response = await fetch("https://api.flywert-dev.ru/api/v1/pages", {
        method: "GET",
        headers: {
          format: "json",
          Accept: "*/*",
        },
      });

      const item = await response.json();
      console.log(item.data[1].content);
      setUrlVideo(item.data[1].content)
    } catch (e) {
      console.error(e);
    } finally {
      setLoading(false)
    }
  };

  useEffect(() => {
    showVideo()
  }, [])

  return (
    <Layout>
      <div className={css.partner}>
        <div className={cx(css.partner__descWrapp, "flex")}>
          <div className={css.partner__video}>
            <div className={css.partner__videoInner}>
              {
                // TODO loader
                !loading && urlVideo !== "" && <ReactPlayer
                  className={css.partner__videoInner}
                  width="100%"
                  height="100%"
                  // url="https://www.youtube.com/watch?v=YAGeohZE0vQ&feature=emb_logo&ab_channel=FlyWertFlywert"
                  // url={url_video}
                  url={urlVideo}
                />
              }

            </div>
          </div>
          <div className={css.partner__desc}>
            <h1>Станьте нашим партнером</h1>
            <div className={css.partner__descInner}>
              <p>
                Европейское образование ценится по всему миру. С данным дипломом
                обычно берут на работу охотнее, чем с другими дипломами. Также
                высока и оплата труда. Вы все ещё сомневаетесь? Учиться в
                Германии, как минимум престижно, а практика в международных
                компаниях во время обучения даст вам старт для новых свершений!
              </p>
              <p>
                Вы самостоятельно выбираете Вузы, и изучаете условия
                поступления. Мы поможем вам оформить все необходимые документы и
                свяжемся с выбранным Вузом лично. Обсудим с вами все нюансы и
                приступим к вашему зачислению в Вуз. Обучение происходит на
                немецком языке.
              </p>
            </div>
          </div>
        </div>

        <div className={cx(css.partner__formWrapp, "flex")}>
          <div className={css.partner__form}>
            {/*<PartnerForm />
            <PartnerFilial />*/}
            <GeneralPartnerForm
              setChosenDiscount={setChosenDiscount}
              setToDate={setToDate}
              setPartnerContacts={setPartnerContacts}
              setDiscountObject={setDiscountObject}
            />
          </div>
          <div className={css.partner__ticket}>
            <h3>Предварительный просмотр билета</h3>
            <div className={css.partner__ticketPreview}>
              <p className={css.partner__ticketPreview_title}>ПОЗДРАВЛЯЕМ!</p>

              <div className={css.partner__ticketPreview_discountWrap}>
                <p className={css.partner__ticketPreview_discount}>Скидка</p>
                <p className={css.partner__ticketPreview_result}>
                  {chosenDiscount}
                </p>
                <p className={css.partner__ticketPreview_name}>
                  {discountObject}
                </p>
              </div>

              <div className={css.partner__ticketPreview_infoWrap}>
                <div className={css.partner__ticketPreview_contacts}>
                  <p className={css.partner__ticketPreview_contactText}>
                    Для получения выигрыша обращайтесь:
                  </p>
                  <p className={css.partner__ticketPreview_contact}>
                    {partnerContacts}
                  </p>
                </div>
                <p>
                  До:{" "}
                  <span className={css.partner__ticketPreview_date}>
                    {toDate}
                  </span>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default Partner;
