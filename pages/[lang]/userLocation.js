import Layout from "@components/layout";
import AlgoliaPlaces from "algolia-places-react";
import Cookies from "js-cookie";
import { useRouter } from "next/router";

export default function userLocation() {
  const router = useRouter();
  return (
    <Layout>
      <AlgoliaPlaces
        placeholder="Write an address here"
        options={{
          appId: process.env.ALGOLIA_APP_ID,
          apiKey: process.env.ALGOLIA_API,
          language: "ru",
          countries: ["ru", "de", "ua"],
          type: "address",
        }}
        onChange={({ query, rawAnswer, suggestion, suggestionIndex }) => {
          const { lat, lng } = suggestion.latlng;
          Cookies.set("lat", lat);
          Cookies.set("lng", lng);
          console.log(`lat: ${lat} lng: ${lng}`);
          router.push("/");
        }}
        onError={({ message }) =>
          console.error(
            "Fired when we could not make the request to Algolia Places servers for any reason but reaching your rate limit."
          )
        }
      />
    </Layout>
  );
}
