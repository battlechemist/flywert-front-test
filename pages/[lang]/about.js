import cx from 'classnames/bind'
import BreadCrumbs from "@components/breadCrumbs";
import Layout from "@components/layout";
import css from '@styles/pages/About.module.scss'

const About = () => {
  return (
      <Layout>
        <div className="about">
          <BreadCrumbs breadCrumbActive="О нас" className={css.aboutPage__breadCrumbs}/>
          <div className={cx(css.aboutPage, "flex")}>
            <div className={css.aboutPage__image}>
              <h1 className={css.aboutPage__mobTitle}>О нас</h1>
              <picture>
                <source srcSet="/img/pages/about/about.jpg" media="(max-width: 480px)" />
                <source srcSet="/img/pages/about/aboutMd.jpg" media="(max-width: 768px)" />
                <img src="/img/pages/about/about.jpg" alt="about page" />
              </picture>
            </div>
            <div className={css.aboutPage__desc}>
              <h1>О нас</h1>
              <div className={css.aboutPage__desc__inner}>
                <p>
                  Европейское образование ценится по всему миру. С данным дипломом обычно берут на работу охотнее, чем с другими дипломами. Также высока
                  и оплата труда. Вы все ещё сомневаетесь? Учиться в Германии, как минимум престижно, а практика в международных компаниях во время обучения даст вам старт для новых свершений!
                </p>
                <p>
                  Вы самостоятельно выбираете Вузы, и изучаете условия поступления. Мы поможем вам оформить все необходимые документы и свяжемся с выбранным Вузом лично. Обсудим с вами все нюансы и приступим к вашему зачислению
                  в Вуз. Обучение происходит на немецком языке.
                </p>
                <p>
                  Вы самостоятельно выбираете Вузы, и изучаете условия поступления. Мы поможем вам оформить все необходимые документы и свяжемся с выбранным Вузом лично. Обсудим с вами все нюансы и приступим к вашему зачислению
                  в Вуз. Обучение происходит на немецком языке.
                </p>
              </div>
            </div>
          </div>
        </div>
      </Layout>
  )
}

export default About;