import {useEffect} from 'react'
import {useRouter} from 'next/router'
import {connect} from 'react-redux'
import Modal from "react-modal";
import {ConfirmEmailAction, clearMessage} from '@store/actions/auth'
import {showModal} from '@store/actions/modal'
import {getInitialLocale} from '@translations/getInitialLocale';
import useTranslation from '@hooks/useTranslation'
import cx from "classnames/bind";
import rootModalCss from "@components/modal/RootModal.module.scss";
import css from "@components/modal/registerModal/RegisterModal.module.scss";


Modal.setAppElement('#__next');

const ConfirmEmail = (props) => {
  const router = useRouter();
  const { t } = useTranslation();

  useEffect(() => {
    if(router.query.confirmEmail) {
      props.ConfirmEmailAction(router.query.confirmEmail)
    }
  }, [])

  const handleOnSubmitLogin = () => {
    router.push(`/${getInitialLocale()}`)
    props.showModal('LOGIN_MODAL')
    props.clearMessage()
  }

  const emailResend = () => {
    console.log("emailResend")
  }

  let customStyles = {
    overlay: {
      backgroundColor: 'rgba(34, 34, 34, 0.4)'
    },
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      padding: 0,
      border: 'none',
      background: 'none',
      maxWidth: '770px',
      width: '100%'
    }
  };

  return (
      <Modal
          isOpen={!!props.message}
          style={customStyles}
      >
        <div className={cx(rootModalCss.auth__modal, rootModalCss.confirm__email_modal, css.register__modal, 'flex__center' )}>
          <div className={rootModalCss.modal__content}>
            <div className={cx(
                props.requestStatus ? (rootModalCss.success__message) : (rootModalCss.error__message),
                rootModalCss.form__message_block, rootModalCss.confirm__email_message
            )}>
              <p>{props.message}</p>
            </div>
            {
              props.requestStatus
                  ? <button className={cx(rootModalCss.submit__btn, rootModalCss.confirm__email_btn)} type="submit" onClick={handleOnSubmitLogin}>Перейти к авторизации</button>
                  : <button className={cx(rootModalCss.submit__btn, rootModalCss.confirm__email_btn)} type="submit" onClick={emailResend}>Новая ссылка</button>
            }
          </div>
        </div>
      </Modal>
  )
}

const mapStateToProps = state => (
    {
      message: state.auth.message,
      requestStatus: state.auth.requestStatus,
      confirmEmailStatus: state.auth.confirmEmailStatus
    }
)


export default connect(mapStateToProps, {ConfirmEmailAction, clearMessage, showModal})(ConfirmEmail)

