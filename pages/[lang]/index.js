import React, { useEffect, useState } from "react";
import axios from "axios";
import { connect, useDispatch } from "react-redux";
import withLocale from "@hocs/withLocale";
import { getLocale } from "@services/utils";
import Layout from "@components/layout";
import useTranslation from "@hooks/useTranslation";
import Reviews from "@components/reviews";
import Products from "@components/products";
import Winners from "@components/winners";
import HowWorks from "@components/HowWorks/";
import BannerHome from "@components/bannerHome";
import Chat from "@components/chat";
import { getUserData } from "@store/actions/user";
import { getPaymentType } from "@store/actions/payment";
import Cookies from "js-cookie";
import { token } from "@services/token";
import { SET_PRODUCTS } from "@store/constants/products";
import PartnerBtn from "@components/btn/partnerBtn";
import {GET_CHAT_MESSAGE_ARRAY} from "@store/constants/chat";

const Home = (props) => {
  const { defaultProducts = [], isAuth, reviews, userData, winners, messageData, authUser } = props;
  const [authProducts, setAuthProducts] = useState([]);
  const dispatch = useDispatch();
  const { t } = useTranslation();

  console.log('authUser', authUser)

  const requestData = {
    filters: {
      lat: Cookies.get("lat") || "52.784603",
      lng: Cookies.get("lng") || "20.448843",
      range: "5",
      gender: userData.gender,
      categories: userData.categories,
    },
  };

  useEffect(async () => {
    const response = await fetch(
      "https://api.flywert-dev.ru/api/v1/get-products",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "X-Localization": getLocale(),
          authorization: `Bearer ${Cookies.getJSON("access_token")}`,
          //Authorization: 'Bearer ' + token,
          Accept: "application/json",
        },
        body: JSON.stringify(requestData),
      }
    );
    const item = await response.json();
    setAuthProducts(item.data);

    dispatch({
      type: SET_PRODUCTS,
      payload: item.data,
    });
  }, []);

  useEffect(() => {
    if (isAuth) {
      props.getUserData();
      props.getPaymentType();
    }
  }, [isAuth]);

  return (
    <Layout titleKey="homeTitle" {...props.auth}>
      <div className="contentAndMenu contentAndMenuHome">
        <div className="leftMenu">
          <Products products={userData ? authProducts : defaultProducts} />
          <Winners winners={winners} />
          <PartnerBtn />
        </div>
        <div className="content">
          <BannerHome />
          <HowWorks />
          <Reviews reviews={reviews} />
          <Chat messageData={messageData}/>
          <div className="phoneScreenSize">
            <Winners winners={winners} />
            <PartnerBtn />
          </div>
        </div>
      </div>
    </Layout>
  );
};

Home.getInitialProps = async ({ req, store }) => {
  const instance = axios.create({
    baseURL: "https://api.flywert-dev.ru/api/v1/",
    //baseURL: 'http://127.0.0.1:8000/api/v1/',
    headers: {
      "X-Localization": getLocale(),
      "Content-Type": "application/json",
      accept: "application/json",
      //authorization: `Bearer ${Cookies.getJSON("access_token")}`,
      Authorization: 'Bearer ' + token,
    },
  });

  const requestData = {
    filters: {
      lat: "52.784603",
      lng: "20.448843",
      range: "5",
    },
  };

  const productsData = await instance
    .post("get-products", requestData)
    .then((res) => {
      if (res.data.success) {
        return res.data.data;
      } else {
        return [];
      }
    })
    .catch((error) => error);

  const reviewsData = await instance

    .get("reviews/?limit=7")
    .then((res) => {
      if (res.data.success) {
        return res.data.data;
      } else {
        return [];
      }
    })
    .catch((error) => error);

  const winnersData = await instance
    .get("top-winners")
    .then((res) => {
      if (res.data.success) {
        return res.data.data;
      } else {
        return [];
      }
    })
    .catch((error) => error);

  const messageData = await instance
      .get("user/chat/sharing?limit=20")
      .then((res) => {
        return res.data.data
      })
      .catch((error) => error);

  const authUser = await instance
      .get("user")
      .then((res) => {
        return res.data.data
      })
      .catch((error) => error);

  store.dispatch({
    type: SET_PRODUCTS,
    payload: productsData,
  });

  store.dispatch({
    type: GET_CHAT_MESSAGE_ARRAY,
    payload: messageData,
  });

  return {
    defaultProducts: productsData || [],
    reviews: reviewsData,
    winners: winnersData,
    messageData: messageData,
    authUser: authUser
  };
};

const mapStateToProps = (state) => ({
  isAuth: state.auth.isAuth,
  userData: state.user.userData,
});

export default withLocale(
  connect(mapStateToProps, { getUserData, getPaymentType })(Home)
);
