import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import Cookies from "js-cookie";

export default function usePosition() {
  const router = useRouter();

  const success = ({ coords }) => {
    Cookies.set("lat", coords.latitude);
    Cookies.set("lng", coords.longitude);
  };

  const onError = (error) => {
    console.error(error);
    router.push("/userLocation");
  };

  useEffect(() => {
    const geo = navigator.geolocation;

    if (Cookies.get("lat") && Cookies.get("lng")) {
      // console.log('Inside Cookies.get("lat") && Cookies.get("lng")');
      return;
    }
    // console.log('After Cookies.get("lat") && Cookies.get("lng")');

    if (!geo) {
      router.push("/userLocation");
      return;
    }

    geo.getCurrentPosition(success, onError);
  }, []);

  return {
    lat: Cookies.get("lat"),
    lng: Cookies.get("lng"),
  };
}
